package NHSPRO8.NHSPRO8;

import org.testng.annotations.AfterSuite;

import common.testcases.BaseTestCaseRegression;

public abstract class ExtendedBaseTestCase extends BaseTestCaseRegression {

	// AfterSuite Method Setup emails and cleanup everything
	@AfterSuite
	@Override
	public void cleanUp() {

	}

	@AfterSuite
	@Override
	public void sendEmailWithResults() {

	}
}
