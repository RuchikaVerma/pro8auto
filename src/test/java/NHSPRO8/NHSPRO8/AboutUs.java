package NHSPRO8.NHSPRO8;

import java.util.Properties;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.support.PageFactory;
import org.testng.ITest;
import org.testng.annotations.Factory;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import NHSPRO.Common.Utilities.Constants;
import NHSPRO8.Common.ManageFiles.CVSFileManager;
import NHSPRO8.Common.pageObjects.HomePage;
import NHSPRO8.Models.LoginModel;
import common.testcases.Configurations;

@Listeners(ExtentReporterNG.class)
public class AboutUs extends ExtendedBaseTestCase implements ITest {

	private LoginModel loginObj;
	final String URL = "com.newHomeSource.url";

	@Factory(dataProviderClass = CVSFileManager.class, dataProvider = "getLoginDetails")
	public AboutUs(String emailAddress, String password) {
		Properties prop = Configurations.getPropertiesManager();
		setGlobalUrl(prop.getProperty(URL));
		loginObj = new LoginModel(emailAddress, password);
	}

	@Test
	@Override
	public void executeTest() {
		super.executeTest();
	}

	public void test() {
		driver.manage().timeouts().implicitlyWait(10L, TimeUnit.SECONDS);
		Properties prop = Configurations.getPropertiesManager();
		setGlobalUrl(prop.getProperty(URL));
		HomePage page = PageFactory.initElements(driver, HomePage.class);
		try {
			page.aboutUs(loginObj);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Override
	public String getTestName() {
		return Constants.AboutUsTestCase;
	}
}
