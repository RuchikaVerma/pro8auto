package NHSPRO8.NHSPRO8;

import java.util.Properties;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.support.PageFactory;
import org.testng.ITest;
import org.testng.annotations.Factory;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import NHSPRO.Common.Utilities.Constants;
import NHSPRO8.Common.ManageFiles.CVSFileManager;
import NHSPRO8.Common.pageObjects.HomePage;
import NHSPRO8.Models.ClientModel;
import NHSPRO8.Models.LoginModel;
import common.testcases.Configurations;

@Listeners(ExtentReporterNG.class)
public class SavedListingsHome extends ExtendedBaseTestCase implements ITest {
	private LoginModel loginObj;
	private ClientModel clientObj;
	final String URL = "com.newHomeSource.url";

	@Factory(dataProviderClass = CVSFileManager.class, dataProvider = "getLoginDetails")
	public SavedListingsHome(String emailAddress, String password) {
		Properties prop = Configurations.getPropertiesManager();
		setGlobalUrl(prop.getProperty(URL));
		loginObj = new LoginModel(emailAddress, password);
	}

	@Test
	@Override
	public void executeTest() {
		super.executeTest();
	}

	public void test() {
		driver.manage().timeouts().implicitlyWait(10L, TimeUnit.SECONDS);
		Properties prop = Configurations.getPropertiesManager();
		setGlobalUrl(prop.getProperty(URL));
		HomePage page = PageFactory.initElements(driver, HomePage.class);
		try {
			page.savedListingsHome(loginObj);
		} catch (InterruptedException e) {
			System.out.println("Unable to Save Listing!");
		}
	}

	@Override
	public String getTestName() {
		return Constants.SavedListingsHomeTestCase;
	}

}
