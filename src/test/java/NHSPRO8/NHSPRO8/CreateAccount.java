package NHSPRO8.NHSPRO8;

import java.util.Properties;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.support.PageFactory;
import org.testng.ITest;
import org.testng.annotations.Factory;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import NHSPRO.Common.Utilities.Constants;
import NHSPRO8.Common.ManageFiles.CVSFileManager;
import NHSPRO8.Common.pageObjects.HomePage;
import NHSPRO8.Models.UserModel;
import common.testcases.Configurations;

@Listeners(ExtentReporterNG.class)
public class CreateAccount extends ExtendedBaseTestCase implements ITest {
	private UserModel userObj;
	final String URL = "com.newHomeSource.url";

	@Factory(dataProviderClass = CVSFileManager.class, dataProvider = "getUserDetails")
	public CreateAccount(String emailAddress, String firstName, String lastName, String zipCode, String password,
			String license) {
		Properties prop = Configurations.getPropertiesManager();
		setGlobalUrl(prop.getProperty(URL));
		userObj = new UserModel(emailAddress, firstName, lastName, zipCode, password, license);
	}

	public void test() {
		driver.manage().timeouts().implicitlyWait(10L, TimeUnit.SECONDS);
		Properties prop = Configurations.getPropertiesManager();
		setGlobalUrl(prop.getProperty(URL));
		HomePage page = PageFactory.initElements(driver, HomePage.class);
		page.createAccount(userObj);
	}

	@Test
	@Override
	public void executeTest() {
		super.executeTest();
	}

	@Override
	public String getTestName() {
		return Constants.CreateAccountTestCase;
	}

}
