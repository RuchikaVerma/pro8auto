package NHSPRO8.NHSPRO8;

import java.util.Properties;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.support.PageFactory;
import org.testng.annotations.Factory;
import org.testng.annotations.Test;

import NHSPRO8.Common.ManageFiles.CVSFileManager;
import NHSPRO8.Common.pageObjects.HomePage;
import NHSPRO8.Models.ClientModel;
import NHSPRO8.Models.LoginModel;
import common.testcases.BaseTestCaseRegression;
import common.testcases.Configurations;

public class SavedSearchesHome extends BaseTestCaseRegression {
	private LoginModel loginObj;
	private ClientModel clientObj;

	final String URL = "com.newHomeSource.url";

	@Factory(dataProviderClass = CVSFileManager.class, dataProvider = "getSavedListingsDetails")
	public SavedSearchesHome(String emailAddress, String password, String firstName, String lastName,
			String emailOfClient, String City, String code, String phone) {
		Properties prop = Configurations.getPropertiesManager();
		setGlobalUrl(prop.getProperty(URL));
		loginObj = new LoginModel(emailAddress, password);

	}

	@Test
	@Override
	public void executeTest() {
		super.executeTest();
	}

	public void test() {
		driver.manage().timeouts().implicitlyWait(10L, TimeUnit.SECONDS);
		Properties prop = Configurations.getPropertiesManager();
		setGlobalUrl(prop.getProperty(URL));
		HomePage page = PageFactory.initElements(driver, HomePage.class);
		try {

			page.SavedSearchesHome(loginObj);

		} catch (InterruptedException e) {
			System.out.println("Unable to check saved searches from home!");
		}
	}

}
