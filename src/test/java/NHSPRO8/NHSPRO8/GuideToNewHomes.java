package NHSPRO8.NHSPRO8;

import java.util.Properties;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.support.PageFactory;
import org.testng.ITest;
import org.testng.annotations.Factory;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import NHSPRO.Common.Utilities.Constants;
import NHSPRO8.Common.ManageFiles.CVSFileManager;
import NHSPRO8.Common.pageObjects.HomePage;
import NHSPRO8.Models.LoginModel;
import NHSPRO8.Models.SearchModel;
import common.testcases.Configurations;

@Listeners(ExtentReporterNG.class)
public class GuideToNewHomes extends ExtendedBaseTestCase implements ITest {

	private LoginModel loginObj;
	private SearchModel searchObj;
	final String URL = "com.newHomeSource.url";

	@Factory(dataProviderClass = CVSFileManager.class, dataProvider = "getSearchDetails")
	public GuideToNewHomes(String emailAddress, String password, String City, String code, String minimumPrice,
			String maximumPrice, String bedRooms, String bathRooms, String area) {
		Properties prop = Configurations.getPropertiesManager();
		setGlobalUrl(prop.getProperty(URL));
		loginObj = new LoginModel(emailAddress, password);
		searchObj = new SearchModel(City, code, minimumPrice, maximumPrice, bedRooms, bathRooms, area);
	}

	@Test
	@Override
	public void executeTest() {
		super.executeTest();
	}

	public void test() {
		driver.manage().timeouts().implicitlyWait(10L, TimeUnit.SECONDS);
		Properties prop = Configurations.getPropertiesManager();
		setGlobalUrl(prop.getProperty(URL));
		HomePage page = PageFactory.initElements(driver, HomePage.class);
		page.GuideToNewHomes(loginObj, searchObj);
	}

	@Override
	public String getTestName() {
		return Constants.GuideToNewHomesTestCase;
	}
}
