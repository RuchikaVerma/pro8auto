package NHSPRO8.NHSPRO8;

import java.util.Properties;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.support.PageFactory;
import org.testng.ITest;
import org.testng.annotations.Factory;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import NHSPRO.Common.Utilities.Constants;
import NHSPRO8.Common.ManageFiles.CVSFileManager;
import NHSPRO8.Common.pageObjects.CommunityResult;
import NHSPRO8.Models.ClientModel;
import NHSPRO8.Models.LoginModel;
import common.testcases.Configurations;

@Listeners(ExtentReporterNG.class)
public class SRP_PrintReport_Homes_Photo extends ExtendedBaseTestCase implements ITest {
	private LoginModel loginObj;
	private ClientModel clientObj;
	final String URL = "com.newHomeSource.url";

	@Factory(dataProviderClass = CVSFileManager.class, dataProvider = "getSavedListingsDetails")
	public SRP_PrintReport_Homes_Photo(String emailAddress, String password, String firstName, String lastName,
			String emailOfClient, String City, String code, String phone) {
		Properties prop = Configurations.getPropertiesManager();
		setGlobalUrl(prop.getProperty(URL));
		loginObj = new LoginModel(emailAddress, password);
		clientObj = new ClientModel(firstName, lastName, emailOfClient, code, City, phone);
	}

	@Test
	@Override
	public void executeTest() {
		super.executeTest();
	}

	public void test() {
		driver.manage().timeouts().implicitlyWait(10L, TimeUnit.SECONDS);
		Properties prop = Configurations.getPropertiesManager();
		setGlobalUrl(prop.getProperty(URL));
		CommunityResult page = PageFactory.initElements(driver, CommunityResult.class);
		page.SRP_PrintReport_Homes_Photo(loginObj, clientObj);
	}

	@Override
	public String getTestName() {
		return Constants.SRPPrintReportHomesListTestCase;
	}

}
