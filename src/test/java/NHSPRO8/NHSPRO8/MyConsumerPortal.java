package NHSPRO8.NHSPRO8;

import java.util.Properties;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.support.PageFactory;
import org.testng.ITest;
import org.testng.annotations.Factory;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import NHSPRO.Common.Utilities.Constants;
import NHSPRO8.Common.ManageFiles.CVSFileManager;
import NHSPRO8.Common.pageObjects.HomePage;
import NHSPRO8.Models.LoginModel;
import NHSPRO8.Models.UploadModel;
import common.testcases.Configurations;

@Listeners(ExtentReporterNG.class)
public class MyConsumerPortal extends ExtendedBaseTestCase implements ITest {
	private LoginModel loginObj;
	private UploadModel uploadObj;
	Properties prop;
	final String URL = "com.newHomeSource.url";

	@Factory(dataProviderClass = CVSFileManager.class, dataProvider = "getLoginDetails")
	public MyConsumerPortal(String emailAddress, String password) {
		prop = Configurations.getPropertiesManager();
		setGlobalUrl(prop.getProperty(URL));
		loginObj = new LoginModel(emailAddress, password);
	}

	@Test
	@Override
	public void executeTest() {
		super.executeTest();
	}

	public void test() {
		driver.manage().timeouts().implicitlyWait(10L, TimeUnit.SECONDS);
		setGlobalUrl(prop.getProperty(URL));
		Properties fileNameProperty = CVSFileManager.getFileNameProperties();
		HomePage page = PageFactory.initElements(driver, HomePage.class);
		System.out.println(fileNameProperty.getProperty("image_upload_path") + " "
				+ fileNameProperty.getProperty("logo_upload_path"));
		uploadObj = new UploadModel(fileNameProperty.getProperty("image_upload_path"),
				fileNameProperty.getProperty("logo_upload_path"));
		try {
			page.myConsumerPortal(loginObj, uploadObj);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@Override
	public String getTestName() {
		return Constants.MyConsumerPortalTestCase;
	}

}
