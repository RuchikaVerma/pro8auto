package NHSPRO8.SRP;

import java.util.Properties;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.support.PageFactory;
import org.testng.ITest;
import org.testng.annotations.Factory;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import NHSPRO.Common.Utilities.Constants;
import NHSPRO8.Common.ManageFiles.CVSFileManager;
import NHSPRO8.Common.pageObjects.SRPPage;
import NHSPRO8.Models.LoginModel;
import NHSPRO8.Models.SearchModel;
import NHSPRO8.NHSPRO8.ExtendedBaseTestCase;
import NHSPRO8.NHSPRO8.ExtentReporterNG;
import common.testcases.Configurations;

@Listeners(ExtentReporterNG.class)
public class VerifySRPLinks extends ExtendedBaseTestCase implements ITest {
	private LoginModel loginObj;
	private SearchModel searchObj;
	final String URL = "com.newHomeSource.url";

	@Factory(dataProviderClass = CVSFileManager.class, dataProvider = "getSearchDetails")
	public VerifySRPLinks(String emailAddress, String password, String City, String code, String minimumPrice,
			String maximumPrice, String bedRooms, String bathRooms, String area) {
		Properties prop = Configurations.getPropertiesManager();
		setGlobalUrl(prop.getProperty(URL));
		loginObj = new LoginModel(emailAddress, password);
		searchObj = new SearchModel(City, code, minimumPrice, maximumPrice, bedRooms, bathRooms, area);
	}

	@Test
	@Override
	public void executeTest() {
		super.executeTest();
	}

	public void test() {
		driver.manage().timeouts().implicitlyWait(10L, TimeUnit.SECONDS);
		Properties prop = Configurations.getPropertiesManager();
		setGlobalUrl(prop.getProperty(URL));
		SRPPage page = PageFactory.initElements(driver, SRPPage.class);
		try {
			page.VerifySRPLinks(loginObj, searchObj);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@Override
	public String getTestName() {
		return Constants.SRPLinksAndVideosTestCase;
	}
}
