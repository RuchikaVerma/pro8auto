package NHSPRO8.CD;

import java.util.Properties;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.support.PageFactory;
import org.testng.ITest;
import org.testng.annotations.Factory;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import NHSPRO.Common.Utilities.Constants;
import NHSPRO8.Common.ManageFiles.CVSFileManager;
import NHSPRO8.Common.pageObjects.CommunityDetails;
import NHSPRO8.Models.ClientModel;
import NHSPRO8.Models.LoginModel;
import NHSPRO8.NHSPRO8.ExtendedBaseTestCase;
import NHSPRO8.NHSPRO8.ExtentReporterNG;
import common.testcases.Configurations;

@Listeners(ExtentReporterNG.class)
public class CD_Gallery_FIV extends ExtendedBaseTestCase implements ITest {
	private LoginModel loginObj;
	private ClientModel clientObj;
	final String URL = "com.newHomeSource.url";

	@Factory(dataProviderClass = CVSFileManager.class, dataProvider = "getSavedListingsDetails")
	public CD_Gallery_FIV(String emailAddress, String password, String firstName, String lastName, String emailOfClient,
			String City, String code, String phone) {
		Properties prop = Configurations.getPropertiesManager();
		setGlobalUrl(prop.getProperty(URL));
		loginObj = new LoginModel(emailAddress, password);
		clientObj = new ClientModel(firstName, lastName, emailOfClient, code, City, phone);
	}

	@Test
	@Override
	public void executeTest() {
		super.executeTest();
	}

	public void test() {
		driver.manage().timeouts().implicitlyWait(10L, TimeUnit.SECONDS);
		Properties prop = Configurations.getPropertiesManager();
		setGlobalUrl(prop.getProperty(URL));
		CommunityDetails page = PageFactory.initElements(driver, CommunityDetails.class);
		page.CD_Gallery_FIV(loginObj, clientObj);
	}

	@Override
	public String getTestName() {
		return Constants.CD_Gallery_FIV;
	}

}
