package NHSPRO8.HD;

import java.util.Properties;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.support.PageFactory;
import org.testng.ITest;
import org.testng.annotations.Factory;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import NHSPRO.Common.Utilities.Constants;
import NHSPRO8.Common.ManageFiles.CVSFileManager;
import NHSPRO8.Common.pageObjects.HomeDetails;
import NHSPRO8.Models.ClientModel;
import NHSPRO8.Models.LoginModel;
import NHSPRO8.NHSPRO8.ExtendedBaseTestCase;
import NHSPRO8.NHSPRO8.ExtentReporterNG;
import common.testcases.Configurations;

@Listeners(ExtentReporterNG.class)
public class HD_MapandDirection extends ExtendedBaseTestCase implements ITest {
	private LoginModel loginObj;
	private ClientModel clientObj;
	final String URL = "com.newHomeSource.url";

	@Factory(dataProviderClass = CVSFileManager.class, dataProvider = "getSavedListingsDetails")
	public HD_MapandDirection(String emailAddress, String password, String firstName, String lastName,
			String emailOfClient, String City, String code, String phone) {
		Properties prop = Configurations.getPropertiesManager();
		setGlobalUrl(prop.getProperty(URL));
		loginObj = new LoginModel(emailAddress, password);
		clientObj = new ClientModel(firstName, lastName, emailOfClient, code, City, phone);
	}

	@Test
	@Override
	public void executeTest() {
		super.executeTest();
	}

	public void test() {
		driver.manage().timeouts().implicitlyWait(10L, TimeUnit.SECONDS);
		Properties prop = Configurations.getPropertiesManager();
		setGlobalUrl(prop.getProperty(URL));
		HomeDetails page = PageFactory.initElements(driver, HomeDetails.class);
		page.HD_MapandDirection(loginObj, clientObj);
	}

	@Override
	public String getTestName() {
		return Constants.HD_MapandDirection;
	}

}
