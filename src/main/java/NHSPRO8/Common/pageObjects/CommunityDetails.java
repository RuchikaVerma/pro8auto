package NHSPRO8.Common.pageObjects;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Random;
import java.util.concurrent.TimeUnit;

import javax.imageio.ImageIO;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;

import com.google.api.client.repackaged.com.google.common.base.Strings;

import NHSPRO8.Common.ManageFiles.CommonFunctions;
import NHSPRO8.Models.ClientModel;
import NHSPRO8.Models.LoginModel;
import NHSPRO8.Models.SearchModel;
import ru.yandex.qatools.ashot.AShot;
import ru.yandex.qatools.ashot.Screenshot;
import ru.yandex.qatools.ashot.shooting.ShootingStrategies;

public class CommunityDetails extends BasePageObject {

	public CommunityDetails(WebDriver driver) {
		super(driver);
		// TODO Auto-generated constructor stub

	}

	private String getCountRegex = "\\d+";

	private By searchResultSummarySelectorPRO = By.cssSelector("#breadcrumbCount");
	private By searchResultSummarySelectorComm;

	WebDriverWait wait = new WebDriverWait(driver, 10L);
	// For community in pro
	// driver.manage().timeouts().implicitlyWait(10L, TimeUnit.SECONDS);
	@FindBy(how = How.XPATH, xpath = ".//*[@id='results']/div/div[3]/ul[2]/li[2]/label")
	private WebElement communitytab;

	private By searchResultSummarySelector = By.id("totalCounts");

	private WebElement searchResultSummary;
	// Element to store community count after clicking community tab
	private WebElement Commcount;

	// This is a box displayed in PRO that is shown when there are 0 homes
	// (Coming soon communities)
	private By searchResultAllListingsSelector = By.cssSelector("#nhs_ResNoMatchBox li > a");
	private WebElement searchResultAllListings;

	@FindBy(id = "facet-save-search-btn")
	private WebElement findHomesButton;

	@FindBy(className = "ui-autocomplete-input")
	protected WebElement searchTextField;
	@FindBy(css = "#HomeSearchBtn")
	protected WebElement homeSearchBtnField;
	@FindBy(id = "breadcrumbCount")
	protected WebElement countField;
	@FindBy(id = "email")
	protected WebElement emailField;
	@FindBy(id = "Password") // Password Textbox
	protected WebElement passwordfield;
	@FindBy(id = "btnsubmit")
	protected WebElement submitButton;
	@FindBy(xpath = ".//*[@id='header-acct']/a") // SignIn link from header
	WebElement loginButton;
	@FindBy(css = "#hide-engage-your-clients.k-widget.k-tooltip")
	WebElement tooltipmodal;
	@FindBy(css = "#facet-save-search-btn")
	WebElement savesearchbutton;
	@FindBy(css = "#name")
	WebElement savesearchname;
	@FindBy(xpath = ".//*[@id='select-or-create-client']/span/span/span[2]")
	WebElement clientdd;
	@FindBy(xpath = ".//*[@id='dropdownlist_listbox']/li[1]")
	WebElement newclient;
	@FindBy(xpath = ".//*[@id='dropdownlist_listbox']/li[2]")
	WebElement Me;
	@FindBy(xpath = ".//*[@id='dropdownlist_listbox']/li[4]")
	WebElement randomclient;
	@FindBy(xpath = ".//*[@id='save']")
	WebElement savebtn;
	@FindBy(className = "swal2-confirm")
	protected WebElement okBtn;

	@FindBy(id = "FirstName")
	protected WebElement firstNameInput;
	@FindBy(id = "first-name")
	protected WebElement firstNameInputPopUp;
	@FindBy(id = "LastName")
	protected WebElement lastNameInput;
	@FindBy(id = "last-name")
	protected WebElement lastNameInputPopUp;
	@FindBy(id = "ZipCode")
	protected WebElement zipCodeInput;
	@FindBy(id = "Password")
	protected WebElement passwordInput;
	@FindBy(id = "PasswordConfirm")
	protected WebElement confirmPasswordInput;
	@FindBy(id = "phone")
	protected WebElement PhoneInput;
	// @FindBy(id = "user-name-menu")

	@FindBy(id = "printReport")
	protected WebElement PrintReport;
	@FindBy(xpath = ".//*[@id='gridContent']/div/table/tbody/tr/td[1]/label")
	protected WebElement CheckboxFirst;
	@FindBy(xpath = ".//*[@id='gridContent']/div/table/tbody/tr[2]/td[1]/label")
	protected WebElement CheckboxSecond;

	@FindBy(id = "print-report-agent-version")
	protected WebElement PrintReportAgent;
	@FindBy(id = "print-report-client-version")
	protected WebElement PrintReportClient;

	@FindBy(xpath = "(.//*[@class='detailPage'])[1]")
	protected WebElement CommunityList;

	@FindBy(id = ".//*[@id='TelerikWindow']/a")
	protected WebElement closebuttonPR;
	@FindBy(id = "photoView")
	protected WebElement PhotoView;

	@FindBy(id = "LocationSearchTextBox")
	protected WebElement LocationSearchbox;

	@FindBy(xpath = ".//*[@id='details-header']/div[1]/div[1]/h1")
	protected WebElement CommunityName;

	@FindBy(xpath = ".//*[@id='details-header']/div[2]/p[1]")
	protected WebElement CommunityPrice;

	@FindBy(xpath = ".//*[@id='comm-detail-back-results']")
	protected WebElement BacktoResult;

	@FindBy(xpath = ".//*[@id='message-contact-to-builder']")
	protected WebElement CD_MessageBuilder;

	@FindBy(css = "#message-request-an-appointment")
	protected WebElement RequestAppoint;

	@FindBy(xpath = "(.//a[@id='print-report-preview'])[1]")
	protected WebElement CD_PrintReportCommunity;

	@FindBy(css = ".results-photo-item:first-child  #print-report-preview")
	protected WebElement PhotoView1;

	@FindBy(css = "#send-request-an-appointment")
	protected WebElement SendEmailBuilderBtn;

	@FindBy(css = "#send-contact-to-builder")
	protected WebElement SendBuilder;

	@FindBy(xpath = ".//*[@id='jump-link-home-details']")
	protected WebElement CD_Homes;

	@FindBy(xpath = ".//*[@id='grid']/table/tbody/tr[1]/td[1]/label")
	protected WebElement HomesCheckboxFirst;

	@FindBy(css = "#gallery-view-map")
	protected WebElement GalleryMap;

	@FindBy(css = "#gallery-view-photos")
	protected WebElement GalleryPhotos;

	@FindBy(css = "#gallery-view-video")
	protected WebElement GalleryVideos;

	@FindBy(css = "#gallery-view-other-media")
	protected WebElement GalleryOtherMedia;

	@FindBy(css = "#gallery-full-screen")
	protected WebElement GalleryFullScreen;

	@FindBy(css = "#big-gallery-close")
	protected WebElement GalleryClose;

	@FindBy(xpath = ".//*[@id='gallery-next']")
	protected WebElement GalleryNext;

	@FindBy(xpath = ".//*[@id='photo-count']")
	protected WebElement Photos;

	@FindBy(css = "#details-gallery-player-box [id=l-gallery]")
	protected WebElement FloorPlan;

	@FindBy(css = "#txtStreetAddress")
	protected WebElement DirectionTextBox;

	@FindBy(css = "#show-route")
	protected WebElement ShowRouteBtn;

	@FindBy(css = ".print-link>a")
	protected WebElement MapPrint;

	protected WebElement saveListingsBtn;
	@FindBy(xpath = ".//*[@id='gridContent']/div/table/tbody/tr/td[1]/label")
	protected WebElement DropDownList;
	@FindBy(xpath = ".//*[@id='dropdownlist-list']/div[3]/ul/li[last()]")
	protected WebElement SearchForClient;
	@FindBy(xpath = ".//*[@id='save-to-listing']/p[2]/span")
	protected WebElement DropdownListClickable;
	@FindBy(xpath = ".//*[@id='search-client']/p/span")
	protected WebElement DropdownListClickableForSearchingName;
	@FindBy(className = "k-textbox")
	protected WebElement SearchTextBoxForCLient;
	@FindBy(xpath = ".//*[@id='clients-list']/div[3]/ul/li")
	protected WebElement ClientsFirstElement;
	@FindBy(id = "guide-to-new-homes-menu")
	protected WebElement GuideToNewHomesLink;
	@FindBy(xpath = ".//*[@id='dropdownlist-list']/div[3]/ul/li[2]")
	protected WebElement DropdownListForMe;
	@FindBy(xpath = ".//*[@id='dropdownlist-list'/div[3]/ul/li[1]")
	protected WebElement DropdownListForNewClient;

	String Error = "";

	// Testing basic UI and community detail data

	public void CD_Basic(LoginModel loginObj, ClientModel clientObj) {

		{
			login(loginObj);
			// TODO Auto-generated method stub
			{
				WebDriverWait wait = new WebDriverWait(driver, 10);
				wait.until(ExpectedConditions.invisibilityOfElementLocated(By.id("account-password")));
				System.out.println(clientObj.City + ", " + clientObj.StateCode);
				searchTextField.sendKeys(clientObj.City + ", " + clientObj.StateCode);

				homeSearchBtnField.click();
				wait.until(ExpectedConditions
						.visibilityOfElementLocated(By.cssSelector("#hide-engage-your-clients.k-widget.k-tooltip")));

				driver.findElement(By.linkText("Close")).click();

				wait.until(ExpectedConditions.invisibilityOfElementLocated(By.className("facets-loading")));

				wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(".//*[@id='breadcrumbCount']")));
				communitytab.click();
				wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(".//*[@id='breadcrumbCount']")));

				wait.until(
						ExpectedConditions.presenceOfElementLocated(By.cssSelector("#gridContent .k-state-selected")));

				System.out.println("community tab selected");
				CheckboxFirst.click();

				CommunityList.click();

				wait.until(ExpectedConditions
						.visibilityOfElementLocated(By.xpath(".//*[@id='comm-detail-back-results']")));

				System.out.println("The community name is " + CommunityName.getText());
				System.out.println("The community Price is " + CommunityPrice.getText());

				Calendar calendar = Calendar.getInstance();
				SimpleDateFormat formater = new SimpleDateFormat("dd_MM_yyyy_hh_mm_ss");

				Screenshot screenshot = new AShot().shootingStrategy(ShootingStrategies.viewportPasting(2000))
						.takeScreenshot(driver);
				try {
					ImageIO.write(screenshot.getImage(), "PNG",
							new File("test-output\\Images\\CDUI_" + formater.format(calendar.getTime()) + "_.png"));
				} catch (IOException e) {
					Assert.fail(e.getMessage());
				} catch (Exception e) {
					Assert.fail(e.getMessage());
				}

				System.out.println("The URL of community detail page is " + driver.getCurrentUrl());

			}

		}

	}

	// CD Page back to search result
	public void BacktoSearchResult(LoginModel loginObj, ClientModel clientObj) throws InterruptedException {

		{
			login(loginObj);

			{
				WebDriverWait wait = new WebDriverWait(driver, 10);
				wait.until(ExpectedConditions.invisibilityOfElementLocated(By.id("account-password")));
				System.out.println(clientObj.City + ", " + clientObj.StateCode);
				// wait.until(ExpectedConditions.invisibilityOfElementLocated(By.id("AllNew")));
				searchTextField.sendKeys(clientObj.City + ", " + clientObj.StateCode);

				homeSearchBtnField.click();
				wait.until(ExpectedConditions
						.visibilityOfElementLocated(By.cssSelector("#hide-engage-your-clients.k-widget.k-tooltip")));

				driver.findElement(By.linkText("Close")).click();

				wait.until(ExpectedConditions.invisibilityOfElementLocated(By.className("facets-loading")));

				wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(".//*[@id='breadcrumbCount']")));
				communitytab.click();
				wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(".//*[@id='breadcrumbCount']")));
				Thread.sleep(2000);
				String txt = driver.findElement(By.xpath(".//*[@id='breadcrumbCount']")).getText();

				wait.until(
						ExpectedConditions.presenceOfElementLocated(By.cssSelector("#gridContent .k-state-selected")));

				System.out.println("community tab selected");

				CheckboxFirst.click();
				String URLOld = "http://www.newhomesourceprofessional.com/communities/texas/austin/city-austin";

				CommunityList.click();

				wait.until(ExpectedConditions
						.visibilityOfElementLocated(By.xpath(".//*[@id='comm-detail-back-results']")));

				BacktoResult.click();
				wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(".//*[@id='breadcrumbCount']")));
				Thread.sleep(2000);
				String CommunityCount2 = driver.findElement(By.xpath(".//*[@id='breadcrumbCount']")).getText();
				String URL = driver.getCurrentUrl();
				System.out.println(URL);
				Assert.assertEquals(URL, URLOld);
				Assert.assertEquals(CommunityCount2, txt);
				System.out.println("Verified and passed");

			}

		}
	}

	public void CD_EmailBuilder(LoginModel loginObj, ClientModel clientObj) {
		// TODO Auto-generated method stub

		{
			login(loginObj);
			{
				try {
					{
						WebDriverWait wait = new WebDriverWait(driver, 10);
						wait.until(ExpectedConditions.invisibilityOfElementLocated(By.id("account-password")));
						System.out.println(clientObj.City + ", " + clientObj.StateCode);
						searchTextField.sendKeys(clientObj.City + ", " + clientObj.StateCode);

						homeSearchBtnField.click();
						wait.until(ExpectedConditions.visibilityOfElementLocated(
								By.cssSelector("#hide-engage-your-clients.k-widget.k-tooltip")));

						driver.findElement(By.linkText("Close")).click();

						wait.until(ExpectedConditions.invisibilityOfElementLocated(By.className("facets-loading")));

						wait.until(
								ExpectedConditions.visibilityOfElementLocated(By.xpath(".//*[@id='breadcrumbCount']")));
						communitytab.click();
						wait.until(
								ExpectedConditions.visibilityOfElementLocated(By.xpath(".//*[@id='breadcrumbCount']")));

						wait.until(ExpectedConditions
								.presenceOfElementLocated(By.cssSelector("#gridContent .k-state-selected")));

						System.out.println("community tab selected");
						CheckboxFirst.click();

						CommunityList.click();

						wait.until(ExpectedConditions
								.visibilityOfElementLocated(By.xpath(".//*[@id='comm-detail-back-results']")));

						Actions action = new Actions(driver);
						action.moveToElement(CD_MessageBuilder).click().sendKeys("Testing cd page send to builder")
								.moveToElement(SendBuilder).click().build().perform();

						System.out.println("Testing SRP Email Builder Succesfull");

					}
				} catch (Exception e) {
					Error += "There is an error email builder. The exception details are mentioned here:"
							+ e.toString();
				}
			}

		}
	}

	// CD page request Appointment
	public void RequestAppointment(LoginModel loginObj, ClientModel clientObj) {

		// TODO Auto-generated method stub

		{
			login(loginObj);
			{
				try {
					// TODO Auto-generated method stub
					{
						WebDriverWait wait = new WebDriverWait(driver, 10);
						wait.until(ExpectedConditions.invisibilityOfElementLocated(By.id("account-password")));
						System.out.println(clientObj.City + ", " + clientObj.StateCode);
						// wait.until(ExpectedConditions.invisibilityOfElementLocated(By.id("AllNew")));
						searchTextField.sendKeys(clientObj.City + ", " + clientObj.StateCode);

						homeSearchBtnField.click();
						wait.until(ExpectedConditions.visibilityOfElementLocated(
								By.cssSelector("#hide-engage-your-clients.k-widget.k-tooltip")));

						driver.findElement(By.linkText("Close")).click();

						wait.until(ExpectedConditions.invisibilityOfElementLocated(By.className("facets-loading")));

						wait.until(
								ExpectedConditions.visibilityOfElementLocated(By.xpath(".//*[@id='breadcrumbCount']")));
						communitytab.click();
						wait.until(
								ExpectedConditions.visibilityOfElementLocated(By.xpath(".//*[@id='breadcrumbCount']")));

						wait.until(ExpectedConditions
								.presenceOfElementLocated(By.cssSelector("#gridContent .k-state-selected")));

						System.out.println("community tab selected");
						CheckboxFirst.click();

						CommunityList.click();

						wait.until(ExpectedConditions
								.visibilityOfElementLocated(By.xpath(".//*[@id='comm-detail-back-results']")));

						Actions action = new Actions(driver);
						action.moveToElement(RequestAppoint).click().sendKeys("Testing cd page Request an Appointment")
								.moveToElement(SendEmailBuilderBtn).click().build().perform();

						System.out.println("Testing CD Request appointment Succesfull");

					}
				} catch (Exception e) {
					Error += "There is an error while requesting an appointment. The exception details are mentioned here:"
							+ e.toString();
				}
			}

		}

	}

	// CD Page Community print report
	public void CD_PrintReportCommunity(LoginModel loginObj, ClientModel clientObj) {
		// TODO Auto-generated method stub
		String Error = "";
		login(loginObj);
		{
			WebDriverWait wait = new WebDriverWait(driver, 10);
			wait.until(ExpectedConditions.invisibilityOfElementLocated(By.id("account-password")));
			System.out.println(clientObj.City + ", " + clientObj.StateCode);
			// wait.until(ExpectedConditions.invisibilityOfElementLocated(By.id("AllNew")));
			searchTextField.sendKeys(clientObj.City + ", " + clientObj.StateCode);

			homeSearchBtnField.click();
			wait.until(ExpectedConditions
					.visibilityOfElementLocated(By.cssSelector("#hide-engage-your-clients.k-widget.k-tooltip")));

			driver.findElement(By.linkText("Close")).click();

			wait.until(ExpectedConditions.invisibilityOfElementLocated(By.className("facets-loading")));

			wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(".//*[@id='breadcrumbCount']")));
			communitytab.click();
			wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(".//*[@id='breadcrumbCount']")));

			wait.until(ExpectedConditions.presenceOfElementLocated(By.cssSelector("#gridContent .k-state-selected")));

			System.out.println("community tab selected");
			CheckboxFirst.click();

			CommunityList.click();

			wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(".//*[@id='comm-detail-back-results']")));
			try {
				((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);",
						CD_PrintReportCommunity);
				CD_PrintReportCommunity.click();
				String oldTab = driver.getWindowHandle();
				PrintReportAgent.click();
				ArrayList<String> newTab = new ArrayList<String>(driver.getWindowHandles());
				newTab.remove(oldTab);
				driver.switchTo().window(newTab.get(0));

				Calendar calendar = Calendar.getInstance();
				SimpleDateFormat formater = new SimpleDateFormat("dd_MM_yyyy_hh_mm_ss");

				Screenshot screenshot = new AShot().shootingStrategy(ShootingStrategies.viewportPasting(2000))
						.takeScreenshot(driver);
				try {
					ImageIO.write(screenshot.getImage(), "PNG", new File("test-output\\Images\\CDpage_comm_agentreport_"
							+ formater.format(calendar.getTime()) + "_.png"));
				} catch (IOException e) {
					Assert.fail(e.getMessage());
				} catch (Exception e) {
					Assert.fail(e.getMessage());
				}
				System.out.println("The URL of CD page Agentreport page is " + driver.getCurrentUrl());
				driver.close();
				driver.switchTo().window(oldTab);
				String oldTab1 = driver.getWindowHandle();
				driver.navigate().refresh();
				CD_PrintReportCommunity.click();

				PrintReportClient.click();
				ArrayList<String> newTab1 = new ArrayList<String>(driver.getWindowHandles());
				newTab1.remove(oldTab1);
				driver.switchTo().window(newTab1.get(0));

				Calendar calendar1 = Calendar.getInstance();
				SimpleDateFormat formater1 = new SimpleDateFormat("dd_MM_yyyy_hh_mm_ss");

				Screenshot screenshot1 = new AShot().shootingStrategy(ShootingStrategies.viewportPasting(2000))
						.takeScreenshot(driver);
				try {
					ImageIO.write(screenshot1.getImage(), "PNG", new File("test-output\\Images\\Cd_Comm_Clientreport_"
							+ formater1.format(calendar1.getTime()) + "_.png"));
				} catch (IOException e) {
					Assert.fail(e.getMessage());
				} catch (Exception e) {
					Assert.fail(e.getMessage());
				}
				System.out.println("The URL of CD page ClientReport is " + driver.getCurrentUrl());
				driver.close();
				ArrayList<String> tab = new ArrayList<String>(driver.getWindowHandles());
				driver.switchTo().window(tab.get(0));
			} catch (Exception e) {
				Error += "There is an error in Print Report. The exception details are mentioned here:" + e.toString();
			}
		}
	}

	// CD Page links, images and Videos
	public void CD_VerifyLinksImagesVideos(LoginModel loginObj, SearchModel searchObj) throws InterruptedException {
		// TODO Auto-generated method stub

		login(loginObj);
		WebDriverWait wait = new WebDriverWait(driver, 100);
		wait.until(ExpectedConditions.invisibilityOfElementLocated(By.id("account-password")));

		searchTextField.sendKeys(searchObj.City + ", " + searchObj.Code);
		homeSearchBtnField.click();
		wait.until(ExpectedConditions
				.visibilityOfElementLocated(By.cssSelector("#hide-engage-your-clients.k-widget.k-tooltip")));

		driver.findElement(By.linkText("Close")).click();

		wait.until(ExpectedConditions.invisibilityOfElementLocated(By.className("facets-loading")));

		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(".//*[@id='breadcrumbCount']")));
		communitytab.click();
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(".//*[@id='breadcrumbCount']")));

		wait.until(ExpectedConditions.presenceOfElementLocated(By.cssSelector("#gridContent .k-state-selected")));

		System.out.println("community tab selected");
		CheckboxFirst.click();

		CommunityList.click();

		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(".//*[@id='comm-detail-back-results']")));

		List<String> Errorlinks = VerifyAllLinks("", "");
		List<String> ErrorImages = VerifyAllImages("", "");
		String Error = "";
		if (Errorlinks.size() > 0) {
			Error += "There are some errors in Links. Here we have the list of links:" + Errorlinks.toString();
		}
		if (ErrorImages.size() > 0) {
			Error += "There are some errors in Images. Here we have the list of Images:" + ErrorImages.toString();
		}
		// TODO Video Play and pause to be still implemented
		if (!Strings.isNullOrEmpty(Error)) {
			Assert.fail(Error);
		}
	}

	public List<String> VerifyAllLinks(String id, String className) {
		List<WebElement> elements = new ArrayList<WebElement>();
		List<String> errorElements = new ArrayList<String>();
		if (Strings.isNullOrEmpty(id) && Strings.isNullOrEmpty(className)) {
			elements = driver.findElements(By.cssSelector("a"));
			for (WebElement element : elements) {
				String Url = element.getAttribute("href");
				// Avoided Empty and null images because sometimes blank images
				// are used by plugins//
				if (!Strings.isNullOrEmpty(Url) && !Url.contains("javascript") && !Url.contains("grid")) {
					if (!Strings.isNullOrEmpty(Url)) {
						boolean active = CommonFunctions.verifyURLStatus(Url);
						if (!active) {
							errorElements.add(Url);
						}

					}
				}
			}
		} else if (!Strings.isNullOrEmpty(id)) {
			elements = driver.findElements(By.cssSelector("#" + id));
			for (WebElement element : elements) {
				List<WebElement> anchorTags = element.findElements(By.cssSelector("a"));
				for (WebElement anchorTag : anchorTags) {
					String Url = anchorTag.getAttribute("href");
					// Avoided Empty and null images because sometimes blank
					// images
					// are used by plugins//
					if (!Strings.isNullOrEmpty(Url) && !Url.contains("javascript") && !Url.contains("grid")) {
						if (!Strings.isNullOrEmpty(Url)) {
							boolean active = CommonFunctions.verifyURLStatus(Url);
							if (!active) {
								errorElements.add(Url);
							}
						}
					}
				}
			}
		} else if (!Strings.isNullOrEmpty(className)) {
			elements = driver.findElements(By.cssSelector("." + className));
			for (WebElement element : elements) {
				List<WebElement> anchorTags = element.findElements(By.cssSelector("a"));
				for (WebElement anchorTag : anchorTags) {
					String Url = anchorTag.getAttribute("href");
					// Avoided Empty and null images because sometimes blank
					// images
					// are used by plugins//
					if (!Strings.isNullOrEmpty(Url) && !Url.contains("javascript") && !Url.contains("grid")) {
						if (!Strings.isNullOrEmpty(Url)) {
							boolean active = CommonFunctions.verifyURLStatus(Url);
							if (!active) {
								errorElements.add(Url);
							}
						}
					}
				}
			}
		}
		return errorElements;
	}

	public List<String> VerifyAllImages(String id, String className) {
		List<WebElement> elements = new ArrayList<WebElement>();
		List<String> errorElements = new ArrayList<String>();
		if (Strings.isNullOrEmpty(id) && Strings.isNullOrEmpty(className)) {
			elements = driver.findElements(By.cssSelector("img"));
			for (WebElement element : elements) {
				String Url = element.getAttribute("src");
				// Avoided Empty and null images because sometimes blank images
				// are used by plugins//
				if (!Strings.isNullOrEmpty(Url) && !Url.contains("javascript") && !Url.contains("grid")) {
					boolean active = CommonFunctions.verifyURLStatus(Url);
					if (!active) {
						errorElements.add(Url);
					}
				}
			}
		} else if (!Strings.isNullOrEmpty(id))

		{
			elements = driver.findElements(By.cssSelector("#" + id));
			for (WebElement element : elements) {
				List<WebElement> anchorTags = element.findElements(By.cssSelector("img"));
				for (WebElement anchorTag : anchorTags) {
					String Url = anchorTag.getAttribute("src");
					// Avoided Empty and null images because sometimes blank
					// images
					// are used by plugins//
					if (!Strings.isNullOrEmpty(Url) && !Url.contains("javascript") && !Url.contains("grid")) {
						if (!Strings.isNullOrEmpty(Url)) {
							boolean active = CommonFunctions.verifyURLStatus(Url);
							if (!active) {
								errorElements.add(Url);
							}
						}
					}
				}
			}
		} else if (!Strings.isNullOrEmpty(className)) {
			elements = driver.findElements(By.cssSelector("." + className));
			for (WebElement element : elements) {
				List<WebElement> anchorTags = element.findElements(By.cssSelector("img"));
				for (WebElement anchorTag : anchorTags) {
					String Url = anchorTag.getAttribute("src");
					// Avoided Empty and null images because sometimes blank
					// images
					// are used by plugins//
					if (!Strings.isNullOrEmpty(Url) && !Url.contains("javascript") && !Url.contains("grid")) {
						if (!Strings.isNullOrEmpty(Url)) {
							boolean active = CommonFunctions.verifyURLStatus(Url);
							if (!active) {
								errorElements.add(Url);
							}
						}
					}
				}
			}
		}
		return errorElements;
	}

	// CD Page Homes Print report
	public void CD_HomesPrintReport(LoginModel loginObj, ClientModel clientObj) {
		// TODO Auto-generated method stub

		String Error = "";
		login(loginObj);
		{
			WebDriverWait wait = new WebDriverWait(driver, 10);
			wait.until(ExpectedConditions.invisibilityOfElementLocated(By.id("account-password")));
			System.out.println(clientObj.City + ", " + clientObj.StateCode);
			searchTextField.sendKeys(clientObj.City + ", " + clientObj.StateCode);

			homeSearchBtnField.click();
			wait.until(ExpectedConditions
					.visibilityOfElementLocated(By.cssSelector("#hide-engage-your-clients.k-widget.k-tooltip")));

			driver.findElement(By.linkText("Close")).click();

			wait.until(ExpectedConditions.invisibilityOfElementLocated(By.className("facets-loading")));

			wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(".//*[@id='breadcrumbCount']")));
			communitytab.click();
			wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(".//*[@id='breadcrumbCount']")));

			wait.until(ExpectedConditions.presenceOfElementLocated(By.cssSelector("#gridContent .k-state-selected")));

			System.out.println("community tab selected");
			CheckboxSecond.click();

			CommunityList.click();

			wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(".//*[@id='comm-detail-back-results']")));
			((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);",
					driver.findElement(By.xpath(".//*[@id='details-comm']/div/div[3]/ul")));
			if (driver.findElements(By.className("k-checkbox")).size() >= 1) {

				try {

					HomesCheckboxFirst.click();
					PrintReport.click();
					String oldTab = driver.getWindowHandle();
					PrintReportAgent.click();
					ArrayList<String> newTab = new ArrayList<String>(driver.getWindowHandles());
					newTab.remove(oldTab);
					driver.switchTo().window(newTab.get(0));

					Calendar calendar = Calendar.getInstance();
					SimpleDateFormat formater = new SimpleDateFormat("dd_MM_yyyy_hh_mm_ss");

					Screenshot screenshot = new AShot().shootingStrategy(ShootingStrategies.viewportPasting(2000))
							.takeScreenshot(driver);
					try {
						ImageIO.write(screenshot.getImage(), "PNG", new File("test-output\\Images\\CD_Homes_Agent_"
								+ formater.format(calendar.getTime()) + "_.png"));
					} catch (IOException e) {
						Assert.fail(e.getMessage());
					} catch (Exception e) {
						Assert.fail(e.getMessage());
					}
					System.out.println("The URL of Agentreport page is " + driver.getCurrentUrl());
					driver.close();
					driver.switchTo().window(oldTab);
					String oldTab1 = driver.getWindowHandle();
					driver.navigate().refresh();
					HomesCheckboxFirst.click();
					PrintReport.click();
					PrintReportClient.click();
					ArrayList<String> newTab1 = new ArrayList<String>(driver.getWindowHandles());
					newTab1.remove(oldTab1);
					driver.switchTo().window(newTab1.get(0));

					Calendar calendar1 = Calendar.getInstance();
					SimpleDateFormat formater1 = new SimpleDateFormat("dd_MM_yyyy_hh_mm_ss");

					Screenshot screenshot1 = new AShot().shootingStrategy(ShootingStrategies.viewportPasting(2000))
							.takeScreenshot(driver);
					try {
						ImageIO.write(screenshot1.getImage(), "PNG",
								new File("test-output\\Images\\CD_Homes_Clientreport_"
										+ formater1.format(calendar1.getTime()) + "_.png"));
					} catch (IOException e) {
						Assert.fail(e.getMessage());
					} catch (Exception e) {
						Assert.fail(e.getMessage());
					}
					System.out.println("The URL of ClientReport is " + driver.getCurrentUrl());
					driver.close();
					ArrayList<String> tab = new ArrayList<String>(driver.getWindowHandles());
					driver.switchTo().window(tab.get(0));
				} catch (Exception e) {
					Error += "There is an error in Print Report. The exception details are mentioned here:"
							+ e.toString();
				}
			}

			else {

				System.out.println("No Homes in this community");

			}
		}
	}

	// CD Page Gallery photos

	public void CD_Gallery(LoginModel loginObj, ClientModel clientObj) {
		// TODO Auto-generated method stub
		login(loginObj);
		{
			WebDriverWait wait = new WebDriverWait(driver, 10);
			wait.until(ExpectedConditions.invisibilityOfElementLocated(By.id("account-password")));
			System.out.println(clientObj.City + ", " + clientObj.StateCode);
			searchTextField.sendKeys(clientObj.City + ", " + clientObj.StateCode);

			homeSearchBtnField.click();
			wait.until(ExpectedConditions
					.visibilityOfElementLocated(By.cssSelector("#hide-engage-your-clients.k-widget.k-tooltip")));

			driver.findElement(By.linkText("Close")).click();

			wait.until(ExpectedConditions.invisibilityOfElementLocated(By.className("facets-loading")));

			wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(".//*[@id='breadcrumbCount']")));
			communitytab.click();
			wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(".//*[@id='breadcrumbCount']")));

			wait.until(ExpectedConditions.presenceOfElementLocated(By.cssSelector("#gridContent .k-state-selected")));

			System.out.println("community tab selected");
			CheckboxFirst.click();

			CommunityList.click();

			wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(".//*[@id='comm-detail-back-results']")));
			if (driver.findElements(By.cssSelector("#gallery-view-photos")).size() != 0) {
				try {

					String CountPhotos = GalleryPhotos.getText();

					String result[] = CountPhotos.split("/");

					String returnValue = result[result.length - 1];
					System.out.println("Total Photos Count is   " + returnValue.substring(0, 1));
					int n = Integer.parseInt(returnValue.substring(0, 1));

					for (int i = 0; i < n; i++) {
						Thread.sleep(1000);

						GalleryNext.click();
					}

					System.out.println("Navigation tested and pass");
				} catch (Exception e) {
					Error += "No Photos for this community. The exception details are mentioned here:" + e.toString();
				}
			} else {
				System.out.println("No Photos for this community");
			}
		}
	}

	// CD Page gallery in FIV
	public void CD_Gallery_FIV(LoginModel loginObj, ClientModel clientObj) {
		// TODO Auto-generated method stub
		login(loginObj);
		{
			WebDriverWait wait = new WebDriverWait(driver, 10);
			wait.until(ExpectedConditions.invisibilityOfElementLocated(By.id("account-password")));
			System.out.println(clientObj.City + ", " + clientObj.StateCode);
			// wait.until(ExpectedConditions.invisibilityOfElementLocated(By.id("AllNew")));
			searchTextField.sendKeys(clientObj.City + ", " + clientObj.StateCode);

			homeSearchBtnField.click();
			wait.until(ExpectedConditions
					.visibilityOfElementLocated(By.cssSelector("#hide-engage-your-clients.k-widget.k-tooltip")));

			driver.findElement(By.linkText("Close")).click();

			wait.until(ExpectedConditions.invisibilityOfElementLocated(By.className("facets-loading")));

			wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(".//*[@id='breadcrumbCount']")));
			communitytab.click();
			wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(".//*[@id='breadcrumbCount']")));

			wait.until(ExpectedConditions.presenceOfElementLocated(By.cssSelector("#gridContent .k-state-selected")));

			System.out.println("community tab selected");
			CheckboxFirst.click();

			CommunityList.click();

			wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(".//*[@id='comm-detail-back-results']")));
			if (driver.findElements(By.cssSelector("#gallery-view-photos")).size() != 0) {
				try {

					GalleryFullScreen.click();

					String CountPhotos = GalleryPhotos.getText();

					String result[] = CountPhotos.split("/");

					String returnValue = result[result.length - 1];
					System.out.println("Total Photos Count is   " + returnValue.substring(0, 1));
					int n = Integer.parseInt(returnValue.substring(0, 1));

					for (int i = 0; i < n; i++) {
						Thread.sleep(1000);

						GalleryNext.click();
					}
					GalleryClose.click();

					System.out.println("FIV Navigation tested and pass");

				} catch (Exception e) {
					Error += "No Photos for this community. The exception details are mentioned here:" + e.toString();
				}
			} else {
				System.out.println("No Photos for this community");
			}
		}
	}

	// CD page gallery videos test case

	public void CD_Gallery_Videos(LoginModel loginObj, ClientModel clientObj) {
		// TODO Auto-generated method stub

		login(loginObj);
		{
			WebDriverWait wait = new WebDriverWait(driver, 10);
			wait.until(ExpectedConditions.invisibilityOfElementLocated(By.id("account-password")));
			System.out.println(clientObj.City + ", " + clientObj.StateCode);
			searchTextField.sendKeys(clientObj.City + ", " + clientObj.StateCode);

			homeSearchBtnField.click();
			wait.until(ExpectedConditions
					.visibilityOfElementLocated(By.cssSelector("#hide-engage-your-clients.k-widget.k-tooltip")));

			driver.findElement(By.linkText("Close")).click();

			wait.until(ExpectedConditions.invisibilityOfElementLocated(By.className("facets-loading")));

			wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(".//*[@id='breadcrumbCount']")));
			communitytab.click();
			wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(".//*[@id='breadcrumbCount']")));

			wait.until(ExpectedConditions.presenceOfElementLocated(By.cssSelector("#gridContent .k-state-selected")));

			System.out.println("community tab selected");
			CheckboxFirst.click();

			CommunityList.click();

			wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(".//*[@id='comm-detail-back-results']")));

			if (driver.findElements(By.cssSelector("#gallery-view-video")).size() != 0) {
				try {

					GalleryVideos.click();

					String CountVideos = GalleryVideos.getText();

					String resultVideos[] = CountVideos.split("/");

					String returnVideos = resultVideos[resultVideos.length - 1];
					System.out.println("Total videos Count is   " + returnVideos.substring(0, 1));
					int count_vid = Integer.parseInt(returnVideos.substring(0, 1));

					for (int i = 0; i < count_vid; i++) {
						Thread.sleep(1000);

						GalleryNext.click();
					}

					System.out.println("Videos Navigation tested and pass");
				} catch (Exception e) {
					Error += "No videos for this community. The exception details are mentioned here:" + e.toString();
				}
			} else {
				System.out.println("No Videos for this community");
			}
		}
	}

	// CD page FIV gallery videos test case

	public void CD_Gallery_Videos_FIV(LoginModel loginObj, ClientModel clientObj) {
		// TODO Auto-generated method stub
		login(loginObj);
		{
			WebDriverWait wait = new WebDriverWait(driver, 10);
			wait.until(ExpectedConditions.invisibilityOfElementLocated(By.id("account-password")));
			System.out.println(clientObj.City + ", " + clientObj.StateCode);
			searchTextField.sendKeys(clientObj.City + ", " + clientObj.StateCode);

			homeSearchBtnField.click();
			wait.until(ExpectedConditions
					.visibilityOfElementLocated(By.cssSelector("#hide-engage-your-clients.k-widget.k-tooltip")));

			driver.findElement(By.linkText("Close")).click();

			wait.until(ExpectedConditions.invisibilityOfElementLocated(By.className("facets-loading")));

			wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(".//*[@id='breadcrumbCount']")));
			communitytab.click();
			wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(".//*[@id='breadcrumbCount']")));

			wait.until(ExpectedConditions.presenceOfElementLocated(By.cssSelector("#gridContent .k-state-selected")));

			System.out.println("community tab selected");
			CheckboxFirst.click();

			CommunityList.click();

			wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(".//*[@id='comm-detail-back-results']")));

			if (driver.findElements(By.cssSelector("#gallery-view-video")).size() != 0) {
				try {
					GalleryFullScreen.click();

					GalleryVideos.click();

					String CountVideos = GalleryVideos.getText();

					String resultVideos[] = CountVideos.split("(");

					String returnVideos = resultVideos[resultVideos.length - 1];
					System.out.println("Total videos Count is   " + returnVideos.substring(0, 1));
					int count_vid = Integer.parseInt(returnVideos.substring(0, 1));

					for (int i = 0; i < count_vid; i++) {
						Thread.sleep(1000);

						GalleryNext.click();
					}

					System.out.println("Videos Navigation in FIV view  tested and pass");
				} catch (Exception e) {
					Error += "No videos for this community. The exception details are mentioned here:" + e.toString();
				}
			} else {
				System.out.println("No videos for this community");
			}
		}
	}

	// Gallery view for other media testing
	public void CD_Gallery_OtherMedia(LoginModel loginObj, ClientModel clientObj) {
		// TODO Auto-generated method stub
		login(loginObj);
		{
			WebDriverWait wait = new WebDriverWait(driver, 10);
			wait.until(ExpectedConditions.invisibilityOfElementLocated(By.id("account-password")));
			System.out.println(clientObj.City + ", " + clientObj.StateCode);
			searchTextField.sendKeys(clientObj.City + ", " + clientObj.StateCode);

			homeSearchBtnField.click();
			wait.until(ExpectedConditions
					.visibilityOfElementLocated(By.cssSelector("#hide-engage-your-clients.k-widget.k-tooltip")));

			driver.findElement(By.linkText("Close")).click();

			wait.until(ExpectedConditions.invisibilityOfElementLocated(By.className("facets-loading")));

			wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(".//*[@id='breadcrumbCount']")));
			communitytab.click();
			wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(".//*[@id='breadcrumbCount']")));

			wait.until(ExpectedConditions.presenceOfElementLocated(By.cssSelector("#gridContent .k-state-selected")));

			System.out.println("community tab selected");
			CheckboxFirst.click();

			CommunityList.click();

			wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(".//*[@id='comm-detail-back-results']")));
			if (driver.findElements(By.cssSelector("#gallery-view-other-media")).size() != 0) {
				try {
					GalleryOtherMedia.click();
					String CountVideos = GalleryOtherMedia.getText();

					String resultOtherMedia[] = CountVideos.split("/");

					String returnMedia = resultOtherMedia[resultOtherMedia.length - 1];
					System.out.println("Total other media Count is   " + returnMedia.substring(0, 1));
					int count_Media = Integer.parseInt(returnMedia.substring(0, 1));

					for (int i = 0; i < count_Media; i++) {
						Thread.sleep(1000);

						GalleryNext.click();
					}

					System.out.println("Other Navigation tested and pass");
				} catch (Exception e) {
					Error += "No other media for this community. The exception details are mentioned here:"
							+ e.toString();
				}
			} else {
				System.out.println("No other media for this community");
			}
		}
	}

	// CD page gallery floor plan navigation testing
	public void CD_GalleryFloorPlan(LoginModel loginObj, ClientModel clientObj) {
		// TODO Auto-generated method stub

		login(loginObj);
		{
			WebDriverWait wait = new WebDriverWait(driver, 10);
			wait.until(ExpectedConditions.invisibilityOfElementLocated(By.id("account-password")));
			System.out.println(clientObj.City + ", " + clientObj.StateCode);
			searchTextField.sendKeys(clientObj.City + ", " + clientObj.StateCode);

			homeSearchBtnField.click();
			wait.until(ExpectedConditions
					.visibilityOfElementLocated(By.cssSelector("#hide-engage-your-clients.k-widget.k-tooltip")));

			driver.findElement(By.linkText("Close")).click();

			wait.until(ExpectedConditions.invisibilityOfElementLocated(By.className("facets-loading")));

			wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(".//*[@id='breadcrumbCount']")));
			communitytab.click();
			wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(".//*[@id='breadcrumbCount']")));

			wait.until(ExpectedConditions.presenceOfElementLocated(By.cssSelector("#gridContent .k-state-selected")));

			System.out.println("community tab selected");
			CheckboxFirst.click();

			CommunityList.click();

			wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(".//*[@id='comm-detail-back-results']")));
			if (driver.findElements(By.cssSelector("#gallery-view-other-media")).size() != 0) {
				try {
					GalleryOtherMedia.click();
					Thread.sleep(1000);
					FloorPlan.click();

					System.out.println("floor plan succesfull");
				} catch (Exception e) {
					Error += "No floor plan in gallery:" + e.toString();
				}
			} else {
				System.out.println("No floor plan for this community");
			}
		}
	}

	// CD Page gallery other media in FIV
	public void CD_Gallery_OtherMediaFIV(LoginModel loginObj, ClientModel clientObj) {
		// TODO Auto-generated method stub
		login(loginObj);
		{
			WebDriverWait wait = new WebDriverWait(driver, 10);
			wait.until(ExpectedConditions.invisibilityOfElementLocated(By.id("account-password")));
			System.out.println(clientObj.City + ", " + clientObj.StateCode);
			searchTextField.sendKeys(clientObj.City + ", " + clientObj.StateCode);

			homeSearchBtnField.click();
			wait.until(ExpectedConditions
					.visibilityOfElementLocated(By.cssSelector("#hide-engage-your-clients.k-widget.k-tooltip")));

			driver.findElement(By.linkText("Close")).click();

			wait.until(ExpectedConditions.invisibilityOfElementLocated(By.className("facets-loading")));

			wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(".//*[@id='breadcrumbCount']")));
			communitytab.click();
			wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(".//*[@id='breadcrumbCount']")));

			wait.until(ExpectedConditions.presenceOfElementLocated(By.cssSelector("#gridContent .k-state-selected")));

			System.out.println("community tab selected");
			CheckboxFirst.click();

			CommunityList.click();

			wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(".//*[@id='comm-detail-back-results']")));

			if (driver.findElements(By.cssSelector("#gallery-view-other-media")).size() != 0) {
				try {
					GalleryFullScreen.click();
					GalleryOtherMedia.click();
					String CountVideos = GalleryOtherMedia.getText();

					String resultOtherMedia[] = CountVideos.split("/");

					String returnMedia = resultOtherMedia[resultOtherMedia.length - 1];
					System.out.println("Total other media Count is   " + returnMedia.substring(0, 1));
					int count_Media = Integer.parseInt(returnMedia.substring(0, 1));

					for (int i = 0; i < count_Media; i++) {
						Thread.sleep(1000);

						GalleryNext.click();
					}

					System.out.println("OTHER Navigation in FIV view  tested and pass");
				} catch (Exception e) {
					Error += "No other media for this community. The exception details are mentioned here:"
							+ e.toString();
				}
			} else {
				System.out.println("No other media for this community");
			}
		}
	}

	// CD Page Map and Directions
	public void CD_MapandDirection(LoginModel loginObj, ClientModel clientObj) {
		// TODO Auto-generated method stub

		login(loginObj);
		{

			// TODO Auto-generated method stub
			{
				WebDriverWait wait = new WebDriverWait(driver, 10);
				wait.until(ExpectedConditions.invisibilityOfElementLocated(By.id("account-password")));
				System.out.println(clientObj.City + ", " + clientObj.StateCode);
				searchTextField.sendKeys(clientObj.City + ", " + clientObj.StateCode);

				homeSearchBtnField.click();
				wait.until(ExpectedConditions
						.visibilityOfElementLocated(By.cssSelector("#hide-engage-your-clients.k-widget.k-tooltip")));

				driver.findElement(By.linkText("Close")).click();

				wait.until(ExpectedConditions.invisibilityOfElementLocated(By.className("facets-loading")));

				wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(".//*[@id='breadcrumbCount']")));
				communitytab.click();
				wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(".//*[@id='breadcrumbCount']")));

				wait.until(
						ExpectedConditions.presenceOfElementLocated(By.cssSelector("#gridContent .k-state-selected")));

				System.out.println("community tab selected");
				CheckboxFirst.click();

				CommunityList.click();

				wait.until(ExpectedConditions
						.visibilityOfElementLocated(By.xpath(".//*[@id='comm-detail-back-results']")));

				try {

					((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);",
							driver.findElement(By.cssSelector("#show-route")));

					DirectionTextBox.sendKeys("Dallas, TX, United States");
					Thread.sleep(1000);
					ShowRouteBtn.click();

					Thread.sleep(2000);

					Calendar calendar = Calendar.getInstance();
					SimpleDateFormat formater = new SimpleDateFormat("dd_MM_yyyy_hh_mm_ss");

					File scrFile = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);

					FileUtils.copyFile(scrFile, new File("test-output\\Images\\CD_MapandDirection_"
							+ formater.format(calendar.getTime()) + "_.png"));

					System.out.println("Map and Dircetion Succesfull");

				} catch (Exception e) {
					Error += "There is an error in map and direction. The exception details are mentioned here:"
							+ e.toString();
				}
			}

		}
	}

	// Saved listing in cd page for new clients
	public void CD_SavedLisitngs(LoginModel loginObj, ClientModel clientObj) {
		// TODO Auto-generated method stub
		String Error = "";
		login(loginObj);

		WebDriverWait wait = new WebDriverWait(driver, 10);

		wait.until(ExpectedConditions.invisibilityOfElementLocated(By.id("account-password")));
		System.out.println(clientObj.City + ", " + clientObj.StateCode);
		searchTextField.sendKeys(clientObj.City + ", " + clientObj.StateCode);

		homeSearchBtnField.click();
		wait.until(ExpectedConditions
				.visibilityOfElementLocated(By.cssSelector("#hide-engage-your-clients.k-widget.k-tooltip")));

		driver.findElement(By.linkText("Close")).click();

		wait.until(ExpectedConditions.invisibilityOfElementLocated(By.className("facets-loading")));

		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(".//*[@id='breadcrumbCount']")));
		communitytab.click();
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(".//*[@id='breadcrumbCount']")));

		wait.until(ExpectedConditions.presenceOfElementLocated(By.cssSelector("#gridContent .k-state-selected")));

		System.out.println("community tab selected");
		CheckboxFirst.click();

		CommunityList.click();

		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(".//*[@id='comm-detail-back-results']")));
		try {

			((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);",
					driver.findElement(By.xpath(".//*[@id='save-to-listing']/p[2]/span")));

			DropdownListClickable.click();
			driver.findElement(By.xpath(".//*[@id='dropdownlist-list']/div[3]/ul/li[1]")).click();

			Random randomGenerator = new Random();
			int randomInt = randomGenerator.nextInt(1000);
			emailField.sendKeys("Ruc" + randomInt + "@gmail.com");
			firstNameInputPopUp.sendKeys("name" + randomInt + "test");

			lastNameInputPopUp.sendKeys("kumar" + randomInt + "xyz");
			PhoneInput.sendKeys(clientObj.Phone);
			driver.findElement(By.id("save-saveListing")).click();
			Thread.sleep(2000);
			okBtn.click();
		} catch (Exception e) {
			Error += "There is an error while saving listing for New Client. The exception details are mentioned here:"
					+ e.toString();

		}
	}

	// Saved listings for me in CD Page
	public void CD_SavedListingsMe(LoginModel loginObj, ClientModel clientObj) {
		// TODO Auto-generated method stub
		login(loginObj);

		WebDriverWait wait = new WebDriverWait(driver, 10);

		wait.until(ExpectedConditions.invisibilityOfElementLocated(By.id("account-password")));
		System.out.println(clientObj.City + ", " + clientObj.StateCode);
		searchTextField.sendKeys(clientObj.City + ", " + clientObj.StateCode);

		homeSearchBtnField.click();
		wait.until(ExpectedConditions
				.visibilityOfElementLocated(By.cssSelector("#hide-engage-your-clients.k-widget.k-tooltip")));

		driver.findElement(By.linkText("Close")).click();

		wait.until(ExpectedConditions.invisibilityOfElementLocated(By.className("facets-loading")));

		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(".//*[@id='breadcrumbCount']")));
		communitytab.click();
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(".//*[@id='breadcrumbCount']")));

		wait.until(ExpectedConditions.presenceOfElementLocated(By.cssSelector("#gridContent .k-state-selected")));

		System.out.println("community tab selected");
		CheckboxFirst.click();

		CommunityList.click();

		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(".//*[@id='comm-detail-back-results']")));

		try {

			((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);",
					driver.findElement(By.xpath(".//*[@id='save-to-listing']/p[2]/span")));

			DropdownListClickable.click();
			driver.findElement(By.xpath(".//*[@id='dropdownlist-list']/div[3]/ul/li[2]")).click();

			driver.findElement(By.id("save-saveListing")).click();
			wait.until(ExpectedConditions.presenceOfElementLocated(By.className("swal2-confirm")));
			okBtn.click();
		} catch (Exception e) {
			Error += "There is an error while saving listing for Me. The exception details are mentioned here:"
					+ e.toString();

		}
	}

	// Search Client saved listings in CD page
	public void CD_SavedListingsSearchClient(LoginModel loginObj, ClientModel clientObj) {
		// TODO Auto-generated method stub
		login(loginObj);

		WebDriverWait wait = new WebDriverWait(driver, 10);

		wait.until(ExpectedConditions.invisibilityOfElementLocated(By.id("account-password")));
		System.out.println(clientObj.City + ", " + clientObj.StateCode);
		searchTextField.sendKeys(clientObj.City + ", " + clientObj.StateCode);

		homeSearchBtnField.click();
		wait.until(ExpectedConditions
				.visibilityOfElementLocated(By.cssSelector("#hide-engage-your-clients.k-widget.k-tooltip")));

		driver.findElement(By.linkText("Close")).click();

		wait.until(ExpectedConditions.invisibilityOfElementLocated(By.className("facets-loading")));

		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(".//*[@id='breadcrumbCount']")));
		communitytab.click();
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(".//*[@id='breadcrumbCount']")));

		wait.until(ExpectedConditions.presenceOfElementLocated(By.cssSelector("#gridContent .k-state-selected")));

		System.out.println("community tab selected");
		CheckboxFirst.click();

		CommunityList.click();

		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(".//*[@id='comm-detail-back-results']")));
		try {

			((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);",
					driver.findElement(By.xpath(".//*[@id='save-to-listing']/p[2]/span")));

			DropdownListClickable.click();

			SearchForClient.click();
			wait.until(ExpectedConditions.presenceOfNestedElementLocatedBy(DropdownListClickableForSearchingName,
					By.className("k-input")));
			DropdownListClickableForSearchingName.click();
			SearchTextBoxForCLient.sendKeys(clientObj.FirstName.trim());
			wait.withTimeout(2, TimeUnit.SECONDS);
			ClientsFirstElement.click();
			driver.findElement(By.id("save-saveListing")).click();
			okBtn.click();

		} catch (Exception e) {
			Error += "There is an error while saving listing for search client. The exception details are mentioned here:"
					+ e.toString();

		}
	}

	// Saved listing random client test case

	public void CD_SavedListingsRandomClient(LoginModel loginObj, ClientModel clientObj) {
		// TODO Auto-generated method stub
		login(loginObj);

		WebDriverWait wait = new WebDriverWait(driver, 10);

		wait.until(ExpectedConditions.invisibilityOfElementLocated(By.id("account-password")));
		System.out.println(clientObj.City + ", " + clientObj.StateCode);
		searchTextField.sendKeys(clientObj.City + ", " + clientObj.StateCode);

		homeSearchBtnField.click();
		wait.until(ExpectedConditions
				.visibilityOfElementLocated(By.cssSelector("#hide-engage-your-clients.k-widget.k-tooltip")));

		driver.findElement(By.linkText("Close")).click();

		wait.until(ExpectedConditions.invisibilityOfElementLocated(By.className("facets-loading")));

		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(".//*[@id='breadcrumbCount']")));
		communitytab.click();
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(".//*[@id='breadcrumbCount']")));

		wait.until(ExpectedConditions.presenceOfElementLocated(By.cssSelector("#gridContent .k-state-selected")));

		System.out.println("community tab selected");
		CheckboxFirst.click();

		CommunityList.click();

		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(".//*[@id='comm-detail-back-results']")));

		try {

			((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);",
					driver.findElement(By.xpath(".//*[@id='save-to-listing']/p[2]/span")));

			DropdownListClickable.click();
			driver.findElement(By.xpath(".//*[@id='dropdownlist-list']/div[3]/ul/li[3]")).click();

			driver.findElement(By.id("save-saveListing")).click();
			wait.until(ExpectedConditions.presenceOfElementLocated(By.className("swal2-confirm")));
			okBtn.click();
		} catch (Exception e) {
			Error += "There is an error while saving listing for Random old client. The exception details are mentioned here:"
					+ e.toString();

		}

	}
}
