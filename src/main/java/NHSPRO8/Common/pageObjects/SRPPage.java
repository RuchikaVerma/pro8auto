package NHSPRO8.Common.pageObjects;

import java.util.ArrayList;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;

import com.google.api.client.repackaged.com.google.common.base.Strings;

import NHSPRO8.Common.ManageFiles.CommonFunctions;
import NHSPRO8.Models.LoginModel;
import NHSPRO8.Models.SearchModel;

public class SRPPage extends BasePageObject {

	public SRPPage(WebDriver driver) {
		super(driver);
		// TODO Auto-generated constructor stub
	}

	@FindBy(className = "ui-autocomplete-input")
	protected WebElement searchTextField;
	@FindBy(xpath = ".//*[@id='HomeSearchBtn']")
	protected WebElement homeSearchBtnField;
	@FindBy(id = "breadcrumbCount")
	protected WebElement countField;

	@FindBy(id = "facet-price")
	protected WebElement facePriceSummary;
	@FindBy(id = "facet-beds")
	protected WebElement facetBeds;
	@FindBy(id = "facet-baths")
	protected WebElement facetBaths;
	@FindBy(id = "facet-sqft-summary")
	protected WebElement faceSqftSummary;
	@FindBy(css = "#hide-engage-your-clients #close-notification-bar-top")
	protected WebElement closePopupButton;

	// Performing a search
	public void performSearch(LoginModel loginObj, SearchModel searchObj) throws InterruptedException {
		login(loginObj);
		String Error = "";
		try {
			Error = facetsSearch(searchObj);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			Assert.fail(e.toString());

		}
		if (!Strings.isNullOrEmpty(Error)) {
			Assert.fail(Error);
		}
	}

	public String facetsSearch(SearchModel searchObj) throws InterruptedException {
		WebDriverWait wait = new WebDriverWait(driver, 100);
		wait.until(ExpectedConditions.invisibilityOfElementLocated(By.id("account-password")));

		searchTextField.sendKeys(searchObj.City + ", " + searchObj.Code);
		homeSearchBtnField.click();
		wait.until(ExpectedConditions
				.visibilityOfElementLocated(By.cssSelector("#hide-engage-your-clients.k-widget.k-tooltip")));

		driver.findElement(By.linkText("Close")).click();
		driver.navigate().refresh();
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(".//*[@id='breadcrumbCount']")));
		// wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("facet-price")));
		Thread.sleep(3000);
		facePriceSummary.click();
		driver.findElement(By.id("PriceLow")).sendKeys(searchObj.MinimumPrice);
		driver.findElement(By.id("PriceHigh")).sendKeys(searchObj.MaximumPrice);
		facetBeds.click();
		driver.findElement(By.cssSelector("label[for='Beds" + searchObj.bedRooms + "'] ")).click();
		facetBaths.click();
		driver.findElement(By.cssSelector("label[for='Baths" + searchObj.bathRooms + "']")).click();
		faceSqftSummary.click();
		driver.findElement(By.id("SqFtLow")).sendKeys(searchObj.Area);
		driver.findElement(By.id("SqFtHigh")).sendKeys(searchObj.MaximumPrice);
		faceSqftSummary.click();
		wait.until(ExpectedConditions.invisibilityOfElementLocated(By.className("facets-loading")));
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("breadcrumbCount")));
		String txt = countField.getText();
		do {
			txt = countField.getText();
		} while (txt.equalsIgnoreCase("Updating"));
		int TotalCount = Integer.parseInt(txt.split(" ")[0]);
		if (TotalCount > 0) {
			int itemsPerPage = 24;
			itemsPerPage = TotalCount < 24 ? TotalCount : 24;
			boolean priceTestPassed = true;
			boolean areaTestPassed = true;
			boolean bedRoomFilterTestPassed = true;
			boolean bathRoomFilterTestPassed = true;
			for (int i = 1; i <= itemsPerPage; i++) {
				int price = Integer.parseInt(
						driver.findElement(By.xpath(".//*[@id='gridContent']/div/table/tbody/tr[" + i + "]/td[7]"))
								.getText().substring(1).replaceAll(",", ""));
				int area = Integer.parseInt(
						driver.findElement(By.xpath(".//*[@id='gridContent']/div/table/tbody/tr[" + i + "]/td[8]"))
								.getText().replaceAll(",", ""));
				int bedRooms = Integer.parseInt(
						driver.findElement(By.xpath(".//*[@id='gridContent']/div/table/tbody/tr[" + i + "]/td[9]"))
								.getText());
				int bathRooms = Integer.parseInt(
						driver.findElement(By.xpath(".//*[@id='gridContent']/div/table/tbody/tr[" + i + "]/td[10]"))
								.getText());
				if (!(price >= Integer.parseInt(searchObj.MinimumPrice)
						&& price <= Integer.parseInt(searchObj.MaximumPrice))) {
					priceTestPassed = false;
				}
				if (!(area >= Integer.parseInt(searchObj.Area))) {
					areaTestPassed = false;
				}
				if (!(bedRooms >= Integer.parseInt(searchObj.bedRooms))) {
					bedRoomFilterTestPassed = false;
				}
				if (!(bathRooms >= Integer.parseInt(searchObj.bathRooms))) {
					bathRoomFilterTestPassed = false;
				}
			}
			if (priceTestPassed && areaTestPassed && bedRoomFilterTestPassed && bathRoomFilterTestPassed) {
				System.out.println("Filters Tested and Passed!");
				return "";
			} else {
				String Error = "";
				if (!priceTestPassed) {
					Error += "Price filter test did not pass.";
				}
				if (!areaTestPassed) {
					Error += "Area filter test did not pass.";
				}
				if (!bedRoomFilterTestPassed) {
					Error += "Bedroom filter test did not pass.";
				}
				if (!bathRoomFilterTestPassed) {
					Error += "Bathroom filter test did not pass.";
				}
				return Error;
			}

		} else {
			return "No Records found to search in this criteria!!";
		}
	}

	public void VerifySRPLinks(LoginModel loginObj, SearchModel searchObj) throws InterruptedException {
		login(loginObj);
		WebDriverWait wait = new WebDriverWait(driver, 100);
		wait.until(ExpectedConditions.invisibilityOfElementLocated(By.id("account-password")));

		searchTextField.sendKeys(searchObj.City + ", " + searchObj.Code);
		homeSearchBtnField.click();
		wait.until(ExpectedConditions
				.visibilityOfElementLocated(By.cssSelector("#hide-engage-your-clients.k-widget.k-tooltip")));

		driver.findElement(By.linkText("Close")).click();
		Thread.sleep(2000);

		List<String> Errorlinks = VerifyAllLinks("", "");
		List<String> ErrorImages = VerifyAllImages("", "");
		String Error = "";
		if (Errorlinks.size() > 0) {
			Error += "There are some errors in Links. Here we have the list of links:" + Errorlinks.toString();
		}
		if (ErrorImages.size() > 0) {
			Error += "There are some errors in Images. Here we have the list of Images:" + ErrorImages.toString();
		}
		// TODO Video Play and pause to be still implemented
		if (!Strings.isNullOrEmpty(Error)) {
			Assert.fail(Error);
		}

	}

	public List<String> VerifyAllLinks(String id, String className) {
		List<WebElement> elements = new ArrayList<WebElement>();
		List<String> errorElements = new ArrayList<String>();
		if (Strings.isNullOrEmpty(id) && Strings.isNullOrEmpty(className)) {
			elements = driver.findElements(By.cssSelector("a"));
			for (WebElement element : elements) {
				String Url = element.getAttribute("href");
				// Avoided Empty and null images because sometimes blank images
				// are used by plugins//
				if (!Strings.isNullOrEmpty(Url)) {
					boolean active = CommonFunctions.verifyURLStatus(Url);
					if (!active) {
						errorElements.add(Url);
					}
				}
			}
		} else if (!Strings.isNullOrEmpty(id)) {
			elements = driver.findElements(By.cssSelector("#" + id));
			for (WebElement element : elements) {
				List<WebElement> anchorTags = element.findElements(By.cssSelector("a"));
				for (WebElement anchorTag : anchorTags) {
					String Url = anchorTag.getAttribute("href");
					// Avoided Empty and null images because sometimes blank
					// images
					// are used by plugins//
					if (!Strings.isNullOrEmpty(Url)) {
						boolean active = CommonFunctions.verifyURLStatus(Url);
						if (!active) {
							errorElements.add(Url);
						}
					}
				}
			}
		} else if (!Strings.isNullOrEmpty(className)) {
			elements = driver.findElements(By.cssSelector("." + className));
			for (WebElement element : elements) {
				List<WebElement> anchorTags = element.findElements(By.cssSelector("a"));
				for (WebElement anchorTag : anchorTags) {
					String Url = anchorTag.getAttribute("href");
					// Avoided Empty and null images because sometimes blank
					// images
					// are used by plugins//
					if (!Strings.isNullOrEmpty(Url)) {
						boolean active = CommonFunctions.verifyURLStatus(Url);
						if (!active) {
							errorElements.add(Url);
						}
					}
				}
			}
		}
		return errorElements;
	}

	public List<String> VerifyAllImages(String id, String className) {
		List<WebElement> elements = new ArrayList<WebElement>();
		List<String> errorElements = new ArrayList<String>();
		if (Strings.isNullOrEmpty(id) && Strings.isNullOrEmpty(className)) {
			elements = driver.findElements(By.cssSelector("img"));
			for (WebElement element : elements) {
				String Url = element.getAttribute("src");
				// Avoided Empty and null images because sometimes blank images
				// are used by plugins//
				if (!Strings.isNullOrEmpty(Url)) {
					boolean active = CommonFunctions.verifyURLStatus(Url);
					if (!active) {
						errorElements.add(Url);
					}
				}
			}
		} else if (!Strings.isNullOrEmpty(id)) {
			elements = driver.findElements(By.cssSelector("#" + id));
			for (WebElement element : elements) {
				List<WebElement> anchorTags = element.findElements(By.cssSelector("img"));
				for (WebElement anchorTag : anchorTags) {
					String Url = anchorTag.getAttribute("src");
					// Avoided Empty and null images because sometimes blank
					// images
					// are used by plugins//
					if (!Strings.isNullOrEmpty(Url)) {
						boolean active = CommonFunctions.verifyURLStatus(Url);
						if (!active) {
							errorElements.add(Url);
						}
					}
				}
			}
		} else if (!Strings.isNullOrEmpty(className)) {
			elements = driver.findElements(By.cssSelector("." + className));
			for (WebElement element : elements) {
				List<WebElement> anchorTags = element.findElements(By.cssSelector("img"));
				for (WebElement anchorTag : anchorTags) {
					String Url = anchorTag.getAttribute("src");
					// Avoided Empty and null images because sometimes blank
					// images
					// are used by plugins//
					if (!Strings.isNullOrEmpty(Url)) {
						boolean active = CommonFunctions.verifyURLStatus(Url);
						if (!active) {
							errorElements.add(Url);
						}
					}
				}
			}
		}
		return errorElements;
	}
}
