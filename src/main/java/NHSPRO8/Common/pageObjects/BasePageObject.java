package NHSPRO8.Common.pageObjects;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.testng.Assert;

import NHSPRO8.Models.LoginModel;
import common.web.BasePage;

public class BasePageObject extends BasePage {

	public BasePageObject(WebDriver driver) {
		super(driver);
		// TODO Auto-generated constructor stub
	}

	@FindBy(id = "email")
	protected WebElement emailField;
	@FindBy(xpath = ".//*[@id='header-acct']/a") // SignIn link from header
	WebElement loginButton;
	@FindBy(id = "Password") // Password Textbox
	protected WebElement passwordfield;
	@FindBy(id = "btn-login-submit")
	protected WebElement submitButton;
	@FindBy(id = "btn-password-submit")
	protected WebElement passwordSubmitButton;

	// Login Method
	protected void login(LoginModel loginObj) {
		try {
			loginButton.click();
			emailField.sendKeys(loginObj.EmailAddress);
			submitButton.click();
			passwordfield.sendKeys(loginObj.Password);
			passwordSubmitButton.click();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			Assert.fail("Login Denied! Here is the cause of exception: " + e.toString());
		}
	}

}
