package NHSPRO8.Common.pageObjects;

import java.awt.AWTException;
import java.awt.Robot;
import java.awt.Toolkit;
import java.awt.datatransfer.StringSelection;
import java.awt.event.KeyEvent;
import java.io.File;
import java.io.IOException;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.file.Paths;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Random;
import java.util.concurrent.TimeUnit;

import javax.imageio.ImageIO;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;

import com.google.api.client.repackaged.com.google.common.base.Strings;

import NHSPRO8.Common.ManageFiles.CommonFunctions;
import NHSPRO8.Models.ClientModel;
import NHSPRO8.Models.LoginModel;
import NHSPRO8.Models.SearchModel;
import NHSPRO8.Models.UploadModel;
import NHSPRO8.Models.UserModel;
import net.lightbody.bmp.proxy.jetty.start.Main;
import ru.yandex.qatools.ashot.AShot;
import ru.yandex.qatools.ashot.Screenshot;
import ru.yandex.qatools.ashot.shooting.ShootingStrategies;

public class HomePage extends BasePageObject {

	@FindBy(id = "HomeSearchBtn")
	private WebElement findHomesButton;

	@FindBy(xpath = ".//*[@id='header-acct']/a") // SignIn link from header
	WebElement loginButton;
	@FindBy(xpath = ".//*[@id='home-signup']/p[2]/a") // SignUp link
	WebElement signUpButton;
	protected By emailFieldLocator = By.id("email"); // Email Field Textbox
	@FindBy(id = "email")
	protected WebElement emailField;
	@FindBy(id = "Password") // Password Textbox
	protected WebElement passwordfield;
	@FindBy(id = "btn-login-submit")
	protected WebElement submitButton;
	@FindBy(id = "btn-register-submit")
	protected WebElement submitCreateAccntButton;
	@FindBy(id = "FirstName")
	protected WebElement firstNameInput;
	@FindBy(id = "first-name")
	protected WebElement firstNameInputPopUp;
	@FindBy(id = "LastName")
	protected WebElement lastNameInput;
	@FindBy(id = "last-name")
	protected WebElement lastNameInputPopUp;
	@FindBy(id = "ZipCode")
	protected WebElement zipCodeInput;
	@FindBy(id = "RealEstateLicense")
	protected WebElement licenseField;
	@FindBy(id = "Password")
	protected WebElement passwordInput;
	@FindBy(id = "PasswordConfirm")
	protected WebElement confirmPasswordInput;
	@FindBy(id = "phone")
	protected WebElement PhoneInput;
	@FindBy(id = "user-name-menu")
	protected WebElement userNameMenu;
	@FindBy(xpath = ".//*[@id='header-acct-menu']/ul/li/a") // .//*[@id='save-listing-nav-content']/ul/li/a
	protected WebElement myAccount;
	@FindBy(xpath = ".//*[@id='header-acct-menu']/ul/li[2]/a") // .//*[@id='gridContent']/div/table/tbody/tr/td[1]
	protected WebElement myConsumerPortal;
	@FindBy(id = "CreateAccountButton")
	protected WebElement createAccountButton;
	@FindBy(linkText = "About Us")
	protected WebElement aboutUsMenu;
	@FindBy(linkText = "Help")
	protected WebElement helpMenu;
	@FindBy(css = "#imageurl-remove")
	protected WebElement removeImageBtn;
	@FindBy(id = "agencylogo-remove")
	protected WebElement AgencyLogoRemoveBtn;
	@FindBy(id = "my-consumer-portal-save")
	protected WebElement saveButton;
	@FindBy(id = "headShotButton")
	protected WebElement headShotButton;
	@FindBy(id = "logoButton")
	protected WebElement logoButton;
	@FindBy(className = "cropControlCrop")
	protected WebElement CropBtn;
	@FindBy(id = "my_clients")
	protected WebElement myClients;
	@FindBy(className = "k-pager-info")
	protected WebElement TotalInfo;
	@FindBy(id = "btn-new-client")
	protected WebElement NewClientButton;
	@FindBy(id = "save-client")
	protected WebElement saveClientButton;
	@FindBy(className = "swal2-confirm")
	protected WebElement okBtn;
	@FindBy(id = "Email")
	protected WebElement clientEmailField;
	@FindBy(className = "ui-autocomplete-input")
	protected WebElement searchTextField;
	@FindBy(id = "HomeSearchBtn")
	protected WebElement homeSearchBtnField;
	@FindBy(id = "breadcrumbCount")
	protected WebElement countField;
	@FindBy(id = "saveListings")
	protected WebElement saveListingsBtn;
	@FindBy(xpath = ".//*[@id='gridContent']/div/table/tbody/tr/td[1]/label")
	protected WebElement CheckboxFirst;
	@FindBy(xpath = ".//*[@id='gridContent']/div/table/tbody/tr[2]/td[1]/label")
	protected WebElement CheckboxSecond;
	@FindBy(className = "k-dropdown-wrap")
	protected WebElement DropDownList;
	@FindBy(xpath = ".//*[@id='dropdownlist-list']/div[3]/ul/li[last()]")
	protected WebElement SearchForClient;
	@FindBy(xpath = ".//*[@id='save-to-listing']/p[2]/span")
	protected WebElement DropdownListClickable;
	@FindBy(xpath = ".//*[@id='search-client']/p/span")
	protected WebElement DropdownListClickableForSearchingName;
	@FindBy(className = "k-textbox")
	protected WebElement SearchTextBoxForCLient;
	@FindBy(xpath = ".//*[@id='clients-list']/div[3]/ul/li")
	protected WebElement ClientsFirstElement;
	@FindBy(id = "guide-to-new-homes-menu")
	protected WebElement GuideToNewHomesLink;
	@FindBy(xpath = ".//*[@id='dropdownlist-list']/div[3]/ul/li[2]")
	protected WebElement DropdownListForMe;
	@FindBy(xpath = ".//*[@id='dropdownlist-list'/div[3]/ul/li[1]")
	protected WebElement DropdownListForNewClient;
	@FindBy(id = "saved-listing")
	protected WebElement SaveListingMenu;

	@FindBy(id = "saved-searches")
	protected WebElement SaveSearchMenu;

	@FindBy(xpath = ".//*[@id='save-listing-nav-content']/ul/li/a")
	protected WebElement SavedListingMenu;
	@FindBy(css = ".tabs li:nth-child(2) label")
	protected WebElement CommunitiesTab;
	@FindBy(css = ".tabs li:nth-child(1) label")
	protected WebElement HomeTab;

	@FindBy(xpath = ".//*[@id='saved-searches']")
	protected WebElement SaveSearchHomeLink;

	// Parameterized Constructor
	public HomePage(WebDriver driver) {
		super(driver);
	}

	// Method for SignIn in PRO8 Agent Site
	public void Login(LoginModel loginObj) {
		login(loginObj);
	}

	// Method to Create an Account (User Type : Agent)
	public void createAccount(UserModel userObj) {
		loginButton.click();
		Random randomGenerator = new Random();
		int randomInt = randomGenerator.nextInt(1000);
		emailField.sendKeys("RuchikaQA" + randomInt + "@gmail.com");
		submitButton.click();

		try {
			firstNameInput.sendKeys(userObj.FirstName);
			lastNameInput.sendKeys(userObj.LastName);
			zipCodeInput.sendKeys(userObj.ZipCode);
			licenseField.sendKeys(userObj.License);
			passwordInput.sendKeys(userObj.Password);
			confirmPasswordInput.sendKeys(userObj.Password);

			submitCreateAccntButton.click();
			Thread.sleep(3000);
			WebDriverWait wait = new WebDriverWait(driver, 2);
			// wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("FirstName")));
			wait.until(ExpectedConditions.invisibilityOfElementLocated(By.id("AllNew")));
			if (driver.findElements(By.id("FirstName")).size() > 0) {
				Assert.fail("Test Failed - Unable to create an account!");
			}
		} catch (Exception e) {
			Assert.fail("Test Failed - unable to create an account!!" + e.toString());
		}
	}

	// Open and Save My Account Page
	public void myAccount(LoginModel loginObj) {
		login(loginObj);
		Actions action = new Actions(driver);
		action.moveToElement(userNameMenu).moveToElement(myAccount).click().build().perform();
		createAccountButton.click();
	}

	// Open and Save My consumer portal page
	// Open and Save My consumer portal page
	public void myConsumerPortal(LoginModel loginObj, UploadModel uploadObj) throws InterruptedException {
		login(loginObj);
		Actions action = new Actions(driver);
		action.moveToElement(userNameMenu).moveToElement(myConsumerPortal).click().build().perform();
		Thread.sleep(3000);
		try {
			removeImageBtn.click();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			System.out.println("Image already not uploaded! Trying to directly upload the new image.");
		}
		try {
			AgencyLogoRemoveBtn.click();
		} catch (Exception e1) {
			// TODO Auto-generated catch block
			System.out.println("Logo Image already not uploaded! Trying to directly upload the new logo image.");
		}
		headShotButton.click();
		String absolutePath = "";
		URL resource = Main.class.getResource("/UploadControlScript.exe");
		try {
			absolutePath = Paths.get(resource.toURI()).toString();
		} catch (URISyntaxException e) {

			// TODO Auto-generated catch block

			Assert.fail("unable to get the Upload control script used (AUTOIT)" + e.getMessage().toString());
		}
		StringSelection s = new StringSelection(uploadObj.ImageUploadPath);
		Toolkit.getDefaultToolkit().getSystemClipboard().setContents(s, null);
		// For keyevent handle
		try {
			Robot r = new Robot();
			r.keyPress(KeyEvent.VK_ENTER);
			r.keyRelease(KeyEvent.VK_ENTER);
			r.keyPress(KeyEvent.VK_CONTROL);
			r.keyPress(KeyEvent.VK_V);
			r.keyRelease(KeyEvent.VK_V);
			r.keyRelease(KeyEvent.VK_CONTROL);
			r.keyPress(KeyEvent.VK_ENTER);
			r.keyRelease(KeyEvent.VK_ENTER);
		} catch (AWTException e) {

		}
		driver.findElement(By.className("cropControlCrop")).click();
		logoButton.click();
		StringSelection ss = new StringSelection(uploadObj.LogouploadPath);
		Toolkit.getDefaultToolkit().getSystemClipboard().setContents(ss, null);
		// For keyevent handle
		try {
			Robot r = new Robot();
			r.keyPress(KeyEvent.VK_ENTER);
			r.keyRelease(KeyEvent.VK_ENTER);
			r.keyPress(KeyEvent.VK_CONTROL);
			r.keyPress(KeyEvent.VK_V);
			r.keyRelease(KeyEvent.VK_V);
			r.keyRelease(KeyEvent.VK_CONTROL);
			r.keyPress(KeyEvent.VK_ENTER);
			r.keyRelease(KeyEvent.VK_ENTER);
		} catch (AWTException e) {
		}
		CropBtn.click();

		((JavascriptExecutor) driver).executeScript("window.scrollTo(0, document.body.scrollHeight)");
		saveButton.click();
		if (!(driver.findElements(By.className("swal2-confirm")).size() > 0)) {
			Assert.fail("There is an error while submitting the details!");
		}

	}

	// Open and compare About Us page
	public void aboutUs(LoginModel loginObj) {
		{
			login(loginObj);
			Actions action = new Actions(driver);
			action.moveToElement(userNameMenu);
			action.moveToElement(aboutUsMenu);
			action.click().build().perform();
			System.out.println("About us Successfull");
			File src = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
			try {
				// now copy the screenshot to desired location using copyFile
				// method
				FileUtils.copyFile(src, new File("E:\\" + "xyz.png"));
			}

			catch (IOException e) {
				Assert.fail(e.getMessage());

			} catch (Exception e) {
				Assert.fail(e.getMessage());

			}
			System.out.println("The URL of ABout us page is " + driver.getCurrentUrl());

		}
	}

	// My Client page from Home page
	public void myClients(LoginModel loginObj, ClientModel clientObj) throws InterruptedException {
		login(loginObj);
		try {
			WebDriverWait wait = new WebDriverWait(driver, 10);
			wait.until(ExpectedConditions.invisibilityOfElementLocated(By.id("account-password")));
			driver.manage().timeouts().implicitlyWait(10L, TimeUnit.SECONDS);
			myClients.click();
			String BeforeInfoText = TotalInfo.getText();
			int BeforeClientsCount = Integer.parseInt(BeforeInfoText.split(" ")[4]);
			NewClientButton.click();
			firstNameInput.sendKeys(clientObj.FirstName);
			lastNameInput.sendKeys(clientObj.LastName);

			Random randomGenerator = new Random();
			int randomInt = randomGenerator.nextInt(1000);
			clientEmailField.sendKeys("username" + randomInt + "@gmail.com");
			// String randomEmail = randomEmail("myclientstesting", number);
			// clientObj.EmailAddress = randomEmail;
			// clientEmailField.sendKeys(clientObj.EmailAddress);
			saveClientButton.click();
			try {
				WebDriverWait waitAgain = new WebDriverWait(driver, 10);
				waitAgain.until(ExpectedConditions.visibilityOf(okBtn));
			} catch (Exception e) {
				// TODO Auto-generated catch block
				Assert.fail("Email Already Exists!");
			}
			okBtn.click();
			WebDriverWait waitAgains = new WebDriverWait(driver, 10);
			waitAgains.until(ExpectedConditions.visibilityOf(TotalInfo));
			String InfoText = TotalInfo.getText();
			int ClientsCount = Integer.parseInt(InfoText.split(" ")[4]);
		} catch (NumberFormatException e) {
			// TODO Auto-generated catch block
			Assert.fail("There was an issue while testing my clients." + e.toString());
		} catch (Exception e) {
			// TODO Auto-generated catch block
			Assert.fail("There was an issue while testing my clients. Here is the cause of exception: " + e.toString());
		}

	}

	// Saved listing -SRP
	public void savedListings(LoginModel loginObj, ClientModel clientObj) throws InterruptedException {
		String Error = "";
		login(loginObj);

		WebDriverWait wait = new WebDriverWait(driver, 10);

		wait.until(ExpectedConditions.presenceOfElementLocated(By.className("btn-loading")));
		wait.until(ExpectedConditions.invisibilityOfElementLocated(By.className("btn-loading")));
		searchTextField.sendKeys(clientObj.City + ", " + clientObj.StateCode);
		homeSearchBtnField.click();
		wait.until(ExpectedConditions
				.visibilityOfElementLocated(By.cssSelector("#hide-engage-your-clients.k-widget.k-tooltip")));

		driver.findElement(By.linkText("Close")).click();

		wait.until(ExpectedConditions.invisibilityOfElementLocated(By.className("facets-loading")));

		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(".//*[@id='breadcrumbCount']")));
		String txt = countField.getText();

		int count = Integer.parseInt(txt.split(" ")[0]);
		if (count > 0) {
			if (driver.findElements(By.className("k-checkbox")).size() > 1) {
				// Save Listing for new client
				try {
					wait.until(ExpectedConditions
							.presenceOfElementLocated(By.cssSelector("#gridContent .k-state-selected")));
					// wait.until(ExpectedConditions.invisibilityOfElementLocated(By.className("k-overlay")));
					CheckboxFirst.click();
					CheckboxSecond.click();
					saveListingsBtn.click();
					wait.until(ExpectedConditions.presenceOfNestedElementLocatedBy(DropdownListClickable,
							By.className("k-input")));
					DropdownListClickable.click();
					driver.findElement(By.xpath(".//*[@id='dropdownlist-list']/div[3]/ul/li[1]")).click();
					Random randomGenerator = new Random();
					int randomInt = randomGenerator.nextInt(1000);
					emailField.sendKeys("Ruc" + randomInt + "@gmail.com");
					firstNameInputPopUp.sendKeys("name" + randomInt + "test");

					lastNameInputPopUp.sendKeys("kumar" + randomInt + "xyz");
					PhoneInput.sendKeys(clientObj.Phone);
					driver.findElement(By.id("save-saveListing")).click();
					Thread.sleep(2000);
					okBtn.click();
				} catch (Exception e) {
					Error += "There is an error while saving listing for New Client. The exception details are mentioned here:"
							+ e.toString();
				}
				// For searching the Name

				// Save Listing for me
				try {
					wait.until(ExpectedConditions.invisibilityOfElementLocated(By.className("k-overlay")));
					CheckboxFirst.click();
					CheckboxSecond.click();
					saveListingsBtn.click();
					wait.until(ExpectedConditions.presenceOfNestedElementLocatedBy(DropdownListClickable,
							By.className("k-input")));
					DropdownListClickable.click();
					driver.findElement(By.xpath(".//*[@id='dropdownlist-list']/div[3]/ul/li[2]")).click();
					driver.findElement(By.id("save-saveListing")).click();
					wait.until(
							ExpectedConditions.invisibilityOfElementLocated(By.xpath(".//*[@id='save-to-listing']")));
					okBtn.click();
					System.out.println("Saved listing me success");
				} catch (Exception e) {
					Error += "There is an error while saving listing for me. The exception details are mentioned here:"
							+ e.toString();
				}

				if (!Strings.isNullOrEmpty(Error)) {
					Assert.fail(Error);
				}

			} else {
				Assert.fail("No Listing found with this search!!");
			}
		} else {
			Assert.fail("No Listing found with this search!!");

		}
		// } catch (Exception ex) {
		// Assert.fail("There was an issue while saving Listings. Here is the
		// cause:" + ex.toString());
		// }

	}

	// Saved Listing Home
	public void savedListingsHome(LoginModel loginObj) throws InterruptedException {
		String Error = "";
		login(loginObj);

		try {
			WebDriverWait wait = new WebDriverWait(driver, 5);
			wait.until(ExpectedConditions.invisibilityOfElementLocated(By.id("AllNew")));
			String oldTab = driver.getWindowHandle();
			Actions action = new Actions(driver);
			int AllElementsCount = driver.findElements(By.cssSelector("#save-listing-nav-content ul> li")).size();
			List<Integer> HomesCount = new ArrayList<Integer>();
			for (int i = 1; i <= AllElementsCount; i++) {
				WebElement Record = driver
						.findElement(By.cssSelector("#save-listing-nav-content ul li:nth-child(" + i + ") a"));// .//*[@id='save-listing-nav-content']/ul/li/a
				action.moveToElement(SaveListingMenu).build().perform();
				action.moveToElement(SaveListingMenu).keyDown(Keys.CONTROL).click(Record).keyUp(Keys.CONTROL).build()
						.perform();
				ArrayList<String> tabs2 = new ArrayList<String>(driver.getWindowHandles());
				tabs2.remove(oldTab);
				// change focus to new tab
				driver.switchTo().window(tabs2.get(0));
				try {
					wait.until(ExpectedConditions.visibilityOfElementLocated(By.className("detailPage")));
					String HomesCountText = driver.findElement(By.className("k-pager-info")).getText();
					System.out.println(HomesCountText);
					String[] HomesSplitted = HomesCountText.split(" ");
					if (HomesSplitted.length > 4) {
						HomesCount.add(Integer.parseInt(HomesSplitted[4].toString()));
					} else {
						HomesCount.add(0);
					}
				} catch (Exception e) {
					HomesCount.add(0);
				}
				if (i == AllElementsCount) {
					CommunitiesTab.click();
					Thread.sleep(2000);
				}
				driver.close();
				ArrayList<String> tab = new ArrayList<String>(driver.getWindowHandles());
				driver.switchTo().window(tab.get(0));
			}
			for (int i = 1; i <= AllElementsCount; i++) {
				WebElement Record = driver
						.findElement(By.cssSelector("#save-listing-nav-content ul li:nth-child(" + i + ") a"));// .//*[@id='save-listing-nav-content']/ul/li/a
				action.moveToElement(SaveListingMenu).build().perform();
				action.moveToElement(SaveListingMenu).keyDown(Keys.CONTROL).click(Record).keyUp(Keys.CONTROL).build()
						.perform();
				String NameAndCount = Record.getText();
				String[] Splittedcount = NameAndCount.split("\\(");
				int Count = Integer.parseInt(Splittedcount[1].split("\\)")[0]);
				ArrayList<String> tabs2 = new ArrayList<String>(driver.getWindowHandles());
				tabs2.remove(oldTab);
				// change focus to new tab
				driver.switchTo().window(tabs2.get(0));
				int CommunitiesCount = 0, TotalCount = 0;
				try {
					CommunitiesTab.click();
					Thread.sleep(2000);
					wait.until(ExpectedConditions.visibilityOfElementLocated(By.className("detailPage")));
					String CommunitiesCountText = driver.findElement(By.className("k-pager-info")).getText();
					System.out.println(CommunitiesCountText);
					String[] CommunitiesSplitted = CommunitiesCountText.split(" ");
					if (CommunitiesSplitted.length > 4) {
						CommunitiesCount = Integer.parseInt(CommunitiesSplitted[4].toString());
					}
				} catch (Exception e) {
				}
				TotalCount = HomesCount.get(i - 1) + CommunitiesCount;

				if (TotalCount != Count) {
					Error += "The count of the rows doesn't match for this saved listing: Count on home page:"
							+ NameAndCount + " Count on listings page:  " + TotalCount;
				}
				driver.close();
				ArrayList<String> tab = new ArrayList<String>(driver.getWindowHandles());
				driver.switchTo().window(tab.get(0));
			}
			if (!Strings.isNullOrEmpty(Error)) {
				Assert.fail(Error);
			}
		} catch (Exception ex) {
			Assert.fail("There was an issue while saving Listings. Here is the cause:" + ex.toString());
		}

	}

	// Performing a search
	public void performSearch(LoginModel loginObj, SearchModel searchObj) {
		login(loginObj);
		String Error = commonSearch(searchObj);
		if (!Strings.isNullOrEmpty(Error)) {
			Assert.fail(Error);
		}
	}

	public String commonSearch(SearchModel searchObj) {
		WebDriverWait wait = new WebDriverWait(driver, 100);
		wait.until(ExpectedConditions.invisibilityOfElementLocated(By.id("account-password")));

		// driver.navigate().refresh();
		searchTextField.sendKeys(searchObj.City + ", " + searchObj.Code);
		Select dropdown;
		dropdown = new Select(driver.findElement(By.id("PriceLow")));
		dropdown.selectByValue(searchObj.MinimumPrice);
		dropdown = new Select(driver.findElement(By.id("PriceHigh")));
		dropdown.selectByValue(searchObj.MaximumPrice);
		dropdown = new Select(driver.findElement(By.id("BedRooms")));
		dropdown.selectByValue(searchObj.bedRooms);
		dropdown = new Select(driver.findElement(By.id("Bathrooms")));
		dropdown.selectByValue(searchObj.bathRooms);
		dropdown = new Select(driver.findElement(By.id("SqFtLow")));
		dropdown.selectByValue(searchObj.Area);
		homeSearchBtnField.click();
		wait.until(ExpectedConditions.invisibilityOfElementLocated(By.className("facets-loading")));
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("breadcrumbCount")));
		String txt = countField.getText();
		while (txt == "Updating") {
			txt = countField.getText();
		}
		int TotalCount = Integer.parseInt(txt.split(" ")[0]);
		if (TotalCount > 0) {
			int itemsPerPage = 24;
			itemsPerPage = TotalCount < 24 ? TotalCount : 24;
			boolean priceTestPassed = true;
			boolean areaTestPassed = true;
			boolean bedRoomFilterTestPassed = true;
			boolean bathRoomFilterTestPassed = true;
			for (int i = 1; i <= itemsPerPage; i++) {
				int price = Integer.parseInt(
						driver.findElement(By.xpath(".//*[@id='gridContent']/div/table/tbody/tr[" + i + "]/td[7]"))
								.getText().substring(1).replaceAll(",", ""));
				int area = Integer.parseInt(
						driver.findElement(By.xpath(".//*[@id='gridContent']/div/table/tbody/tr[" + i + "]/td[8]"))
								.getText().replaceAll(",", ""));
				int bedRooms = Integer.parseInt(
						driver.findElement(By.xpath(".//*[@id='gridContent']/div/table/tbody/tr[" + i + "]/td[9]"))
								.getText());
				int bathRooms = Integer.parseInt(
						driver.findElement(By.xpath(".//*[@id='gridContent']/div/table/tbody/tr[" + i + "]/td[10]"))
								.getText());
				if (!(price >= Integer.parseInt(searchObj.MinimumPrice)
						&& price <= Integer.parseInt(searchObj.MaximumPrice))) {
					priceTestPassed = false;
				}
				if (!(area >= Integer.parseInt(searchObj.Area))) {
					areaTestPassed = false;
				}
				if (!(bedRooms >= Integer.parseInt(searchObj.bedRooms))) {
					bedRoomFilterTestPassed = false;
				}
				if (!(bathRooms >= Integer.parseInt(searchObj.bathRooms))) {
					bathRoomFilterTestPassed = false;
				}
			}
			if (priceTestPassed && areaTestPassed && bedRoomFilterTestPassed && bathRoomFilterTestPassed) {
				System.out.println("Filters Tested and Passed!");
				return "";
			} else {
				String Error = "";
				if (!priceTestPassed) {
					Error += "Price filter test did not pass.";
				}
				if (!areaTestPassed) {
					Error += "Area filter test did not pass.";
				}
				if (!bedRoomFilterTestPassed) {
					Error += "Bedroom filter test did not pass.";
				}
				if (!bathRoomFilterTestPassed) {
					Error += "Bathroom filter test did not pass.";
				}
				return Error;
			}

		} else {
			return "No Records found to search in this criteria!!";
		}
	}

	// Guide to New Home page,checking all the links and Tags
	public void GuideToNewHomes(LoginModel loginObj, SearchModel searchObj) {
		login(loginObj);
		WebDriverWait wait = new WebDriverWait(driver, 10);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("user-name-menu")));
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("guide-to-new-homes-menu")));
		GuideToNewHomesLink.click();
		List<WebElement> elements = driver.findElements(By.cssSelector(".cms-col-left"));
		elements.addAll(driver.findElements(By.cssSelector(".cms-col-right")));
		elements.addAll(driver.findElements(By.cssSelector(".cms-trending")));
		List<String> ErrorAnchorElementsOnThePage = new ArrayList<String>();
		List<String> ErrorImagesOnThePage = new ArrayList<String>();
		// For anchor tags
		if (elements.size() > 0) {
			for (WebElement element : elements) {
				List<WebElement> anchorTags = element.findElements(By.cssSelector("a"));
				for (WebElement anchorTag : anchorTags) {
					String Url = anchorTag.getAttribute("href");
					if (!Strings.isNullOrEmpty(Url)) {
						boolean active = CommonFunctions.verifyURLStatus(Url);
						if (!active) {
							ErrorAnchorElementsOnThePage.add(Url);
						}
					}
				}
			}
		}

		// For All images
		List<WebElement> images = driver.findElements(By.cssSelector("img"));
		int i = 0;
		if (images.size() > 0) {
			for (WebElement image : images) {
				String Src = image.getAttribute("src");
				// Avoided Empty and null images because sometimes blank images
				// are used by plugins//
				if (!Strings.isNullOrEmpty(Src)) {
					boolean active = CommonFunctions.verifyURLStatus(Src);
					if (!active) {
						ErrorImagesOnThePage.add(Src);
					}
				}
			}
		}
		String Error = "";
		if (ErrorAnchorElementsOnThePage.size() > 0) {
			Error += "Some Anchor tags are resulting into Errors. ErrorElements are listed here:"
					+ ErrorAnchorElementsOnThePage.toString();
		}
		if (ErrorImagesOnThePage.size() > 0) {
			Error += "Some Image tags are resulting into Errors. ErrorElements are listed here:"
					+ ErrorImagesOnThePage.toString();
		}
		Error += " " + commonSearch(searchObj);

	}

	// Open and Compare Help page
	public void help(LoginModel loginObj) throws InterruptedException {
		{
			login(loginObj);
			Actions action = new Actions(driver);
			action.moveToElement(userNameMenu);
			action.moveToElement(helpMenu);
			action.click().build().perform();
			System.out.println("Help Successfull");

			// CommonFunctions.screenshot();
			Calendar calendar = Calendar.getInstance();
			SimpleDateFormat formater = new SimpleDateFormat("dd_MM_yyyy_hh_mm_ss");

			Screenshot screenshot = new AShot().shootingStrategy(ShootingStrategies.viewportPasting(2000))
					.takeScreenshot(driver);
			try {
				ImageIO.write(screenshot.getImage(), "PNG",
						new File("test-output\\Images\\Help_" + formater.format(calendar.getTime()) + "_.png"));
			} catch (IOException e) {
				Assert.fail(e.getMessage());
			} catch (Exception e) {
				Assert.fail(e.getMessage());
			}

			System.out.println("The URL of Help page is " + driver.getCurrentUrl());

		}
	}

	// Home page videos and link checking
	public void HomePageLinksAndVideos() throws InterruptedException {
		List<String> Errorlinks = VerifyAllLinks("", "");
		List<String> ErrorImages = VerifyAllImages("", "");
		String Error = "";
		if (Errorlinks.size() > 0) {
			Error += "There are some errors in Links. Here we have the list of links:" + Errorlinks.toString();
		}
		if (ErrorImages.size() > 0) {
			Error += "There are some errors in Images. Here we have the list of Images:" + ErrorImages.toString();
		}
		// TODO Video Play and pause to be still implemented
		if (!Strings.isNullOrEmpty(Error)) {
			Assert.fail(Error);
		}
	}

	public List<String> VerifyAllLinks(String id, String className) {
		List<WebElement> elements = new ArrayList<WebElement>();
		List<String> errorElements = new ArrayList<String>();
		if (Strings.isNullOrEmpty(id) && Strings.isNullOrEmpty(className)) {
			elements = driver.findElements(By.cssSelector("a"));
			for (WebElement element : elements) {
				String Url = element.getAttribute("href");
				// Avoided Empty and null images because sometimes blank images
				// are used by plugins//
				if (!Strings.isNullOrEmpty(Url)) {
					boolean active = CommonFunctions.verifyURLStatus(Url);
					if (!active) {
						errorElements.add(Url);
					}
				}
			}
		} else if (!Strings.isNullOrEmpty(id)) {
			elements = driver.findElements(By.cssSelector("#" + id));
			for (WebElement element : elements) {
				List<WebElement> anchorTags = element.findElements(By.cssSelector("a"));
				for (WebElement anchorTag : anchorTags) {
					String Url = anchorTag.getAttribute("href");
					// Avoided Empty and null images because sometimes blank
					// images
					// are used by plugins//
					if (!Strings.isNullOrEmpty(Url)) {
						boolean active = CommonFunctions.verifyURLStatus(Url);
						if (!active) {
							errorElements.add(Url);
						}
					}
				}
			}
		} else if (!Strings.isNullOrEmpty(className)) {
			elements = driver.findElements(By.cssSelector("." + className));
			for (WebElement element : elements) {
				List<WebElement> anchorTags = element.findElements(By.cssSelector("a"));
				for (WebElement anchorTag : anchorTags) {
					String Url = anchorTag.getAttribute("href");
					// Avoided Empty and null images because sometimes blank
					// images
					// are used by plugins//
					if (!Strings.isNullOrEmpty(Url)) {
						boolean active = CommonFunctions.verifyURLStatus(Url);
						if (!active) {
							errorElements.add(Url);
						}
					}
				}
			}
		}
		return errorElements;
	}

	public List<String> VerifyAllImages(String id, String className) {
		List<WebElement> elements = new ArrayList<WebElement>();
		List<String> errorElements = new ArrayList<String>();
		if (Strings.isNullOrEmpty(id) && Strings.isNullOrEmpty(className)) {
			elements = driver.findElements(By.cssSelector("img"));
			for (WebElement element : elements) {
				String Url = element.getAttribute("src");
				// Avoided Empty and null images because sometimes blank images
				// are used by plugins//
				if (!Strings.isNullOrEmpty(Url)) {
					boolean active = CommonFunctions.verifyURLStatus(Url);
					if (!active) {
						errorElements.add(Url);
					}
				}
			}
		} else if (!Strings.isNullOrEmpty(id)) {
			elements = driver.findElements(By.cssSelector("#" + id));
			for (WebElement element : elements) {
				List<WebElement> anchorTags = element.findElements(By.cssSelector("img"));
				for (WebElement anchorTag : anchorTags) {
					String Url = anchorTag.getAttribute("src");
					// Avoided Empty and null images because sometimes blank
					// images
					// are used by plugins//
					if (!Strings.isNullOrEmpty(Url)) {
						boolean active = CommonFunctions.verifyURLStatus(Url);
						if (!active) {
							errorElements.add(Url);
						}
					}
				}
			}
		} else if (!Strings.isNullOrEmpty(className)) {
			elements = driver.findElements(By.cssSelector("." + className));
			for (WebElement element : elements) {
				List<WebElement> anchorTags = element.findElements(By.cssSelector("img"));
				for (WebElement anchorTag : anchorTags) {
					String Url = anchorTag.getAttribute("src");
					// Avoided Empty and null images because sometimes blank
					// images
					// are used by plugins//
					if (!Strings.isNullOrEmpty(Url)) {
						boolean active = CommonFunctions.verifyURLStatus(Url);
						if (!active) {
							errorElements.add(Url);
						}
					}
				}
			}
		}
		return errorElements;
	}

	// Saved Search from Home Page
	public void SavedSearchesHome(LoginModel loginObj) throws InterruptedException

	{

		// TODO Auto-generated method stub

		String Error = "";
		login(loginObj);

		try {
			WebDriverWait wait = new WebDriverWait(driver, 10);
			wait.until(ExpectedConditions.invisibilityOfElementLocated(By.id("AllNew")));
			String oldTab = driver.getWindowHandle();
			Actions action = new Actions(driver);
			int AllElementsCount = driver.findElements(By.cssSelector("#saveListingContainer>ul> li")).size();
			for (int i = 1; i <= AllElementsCount; i++) {
				action.moveToElement(SaveSearchMenu).build().perform();
				WebElement Record = driver
						.findElement(By.cssSelector("#saveListingContainer ul li:nth-child(" + i + ") a"));
				String url = Record.getAttribute("href");
				boolean status = CommonFunctions.verifyURLStatus(url);
				if (!status) {
					Error += url + " ";
				}
			}
			if (!Strings.isNullOrEmpty(Error)) {
				Assert.fail("There are some Url's not working on saved searched menu. Url's are:" + Error);
			}

		} catch (

		Exception ex) {
			Assert.fail("There was an issue while saving home from home page. Here is the cause:" + ex.toString());
		}

	}

	// Srp saved lisitngs for new client test case
	public void SRP_SavedListingsSearchClients(LoginModel loginObj, ClientModel clientObj) {

		String Error = "";
		login(loginObj);

		WebDriverWait wait = new WebDriverWait(driver, 10);

		wait.until(ExpectedConditions.invisibilityOfElementLocated(By.id("account-password")));
		System.out.println(clientObj.City + ", " + clientObj.StateCode);
		searchTextField.sendKeys(clientObj.City + ", " + clientObj.StateCode);

		homeSearchBtnField.click();
		wait.until(ExpectedConditions
				.visibilityOfElementLocated(By.cssSelector("#hide-engage-your-clients.k-widget.k-tooltip")));

		driver.findElement(By.linkText("Close")).click();

		wait.until(ExpectedConditions.invisibilityOfElementLocated(By.className("facets-loading")));

		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(".//*[@id='breadcrumbCount']")));
		String txt = countField.getText();

		int count = Integer.parseInt(txt.split(" ")[0]);
		if (count > 0) {
			if (driver.findElements(By.className("k-checkbox")).size() > 1) {

				try {
					wait.until(ExpectedConditions
							.presenceOfElementLocated(By.cssSelector("#gridContent .k-state-selected")));
					// wait.until(ExpectedConditions.invisibilityOfElementLocated(By.className("k-overlay")));
					CheckboxFirst.click();
					CheckboxSecond.click();
					saveListingsBtn.click();
					wait.until(ExpectedConditions.presenceOfNestedElementLocatedBy(DropdownListClickable,
							By.className("k-input")));
					DropdownListClickable.click();
					SearchForClient.click();
					wait.until(ExpectedConditions.presenceOfNestedElementLocatedBy(
							DropdownListClickableForSearchingName, By.className("k-input")));
					DropdownListClickableForSearchingName.click();
					SearchTextBoxForCLient.sendKeys(clientObj.FirstName.trim());
					wait.withTimeout(2, TimeUnit.SECONDS);
					ClientsFirstElement.click();
					driver.findElement(By.id("save-saveListing")).click();
					wait.until(
							ExpectedConditions.invisibilityOfElementLocated(By.xpath(".//*[@id='save-to-listing']")));
					okBtn.click();

				} catch (Exception e) {
					Error += "There is an error while saving listing for New Client. The exception details are mentioned here:"
							+ e.toString();
				}
				// TODO Auto-generated method stub

			}

		}
	}
}
