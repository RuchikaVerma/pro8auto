package NHSPRO8.Common.pageObjects;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Random;

import javax.imageio.ImageIO;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;

import NHSPRO8.Models.ClientModel;
import NHSPRO8.Models.LoginModel;
import ru.yandex.qatools.ashot.AShot;
import ru.yandex.qatools.ashot.Screenshot;
import ru.yandex.qatools.ashot.shooting.ShootingStrategies;

public class CommunityResult extends BasePageObject {

	private String getCountRegex = "\\d+";

	private By searchResultSummarySelectorPRO = By.cssSelector("#breadcrumbCount");
	private By searchResultSummarySelectorComm;

	WebDriverWait wait = new WebDriverWait(driver, 10L);
	// For community in pro
	// driver.manage().timeouts().implicitlyWait(10L, TimeUnit.SECONDS);
	@FindBy(how = How.XPATH, xpath = ".//*[@id='results']/div/div[3]/ul[2]/li[2]/label")
	private WebElement communitytab;

	private By searchResultSummarySelector = By.id("totalCounts");

	private WebElement searchResultSummary;
	// Element to store community count after clicking community tab
	private WebElement Commcount;

	// This is a box displayed in PRO that is shown when there are 0 homes
	// (Coming soon communities)
	private By searchResultAllListingsSelector = By.cssSelector("#nhs_ResNoMatchBox li > a");
	private WebElement searchResultAllListings;

	@FindBy(id = "facet-save-search-btn")
	private WebElement findHomesButton;

	@FindBy(className = "ui-autocomplete-input")
	protected WebElement searchTextField;
	@FindBy(css = "#HomeSearchBtn")
	protected WebElement homeSearchBtnField;
	@FindBy(id = "breadcrumbCount")
	protected WebElement countField;
	@FindBy(id = "email")
	protected WebElement emailField;
	@FindBy(id = "Password") // Password Textbox
	protected WebElement passwordfield;
	@FindBy(id = "btnsubmit")
	protected WebElement submitButton;
	@FindBy(xpath = ".//*[@id='header-acct']/a") // SignIn link from header
	WebElement loginButton;
	@FindBy(css = "#hide-engage-your-clients.k-widget.k-tooltip")
	WebElement tooltipmodal;
	@FindBy(css = "#facet-save-search-btn")
	WebElement savesearchbutton;
	@FindBy(css = "#name")
	WebElement savesearchname;
	@FindBy(xpath = ".//*[@id='select-or-create-client']/span/span/span[2]")
	WebElement clientdd;
	@FindBy(xpath = ".//*[@id='dropdownlist_listbox']/li[1]")
	WebElement newclient;
	@FindBy(xpath = ".//*[@id='dropdownlist_listbox']/li[2]")
	WebElement Me;
	@FindBy(xpath = ".//*[@id='dropdownlist_listbox']/li[4]")
	WebElement randomclient;
	@FindBy(xpath = ".//*[@id='save']")
	WebElement savebtn;
	@FindBy(className = "swal2-confirm")
	protected WebElement okBtn;

	@FindBy(id = "FirstName")
	protected WebElement firstNameInput;
	@FindBy(id = "first-name")
	protected WebElement firstNameInputPopUp;
	@FindBy(id = "LastName")
	protected WebElement lastNameInput;
	@FindBy(id = "last-name")
	protected WebElement lastNameInputPopUp;
	@FindBy(id = "ZipCode")
	protected WebElement zipCodeInput;
	@FindBy(id = "Password")
	protected WebElement passwordInput;
	@FindBy(id = "PasswordConfirm")
	protected WebElement confirmPasswordInput;
	@FindBy(id = "phone")
	protected WebElement PhoneInput;
	// @FindBy(id = "user-name-menu")

	@FindBy(id = "printReport")
	protected WebElement PrintReport;
	@FindBy(xpath = ".//*[@id='gridContent']/div/table/tbody/tr/td[1]/label")
	protected WebElement CheckboxFirst;
	@FindBy(xpath = ".//*[@id='gridContent']/div/table/tbody/tr[2]/td[1]/label")
	protected WebElement CheckboxSecond;

	@FindBy(id = "print-report-agent-version")
	protected WebElement PrintReportAgent;
	@FindBy(id = "print-report-client-version")
	protected WebElement PrintReportClient;

	@FindBy(id = ".//*[@id='TelerikWindow']/a")
	protected WebElement closebuttonPR;
	@FindBy(id = "photoView")
	protected WebElement PhotoView;

	@FindBy(id = "LocationSearchTextBox")
	protected WebElement LocationSearchbox;

	@FindBy(id = ".//*[@id='facet-location-places']/div[2]/label[1]")
	protected WebElement Cities;

	@FindBy(css = ".facet-box-col label:nth-of-type(2)")
	protected WebElement Zipcode;

	@FindBy(css = ".facet-box-col label:nth-of-type(3)")
	protected WebElement Schooldistrict;

	@FindBy(css = ".facet-box-col label:nth-of-type(4)")
	protected WebElement Counties;

	@FindBy(id = ".//*[@id='CitiesRadioFacets']/ul[1]/li[1]/label")
	protected WebElement Citycheck;

	@FindBy(id = ".//*[@id='facets-clear']/a")
	protected WebElement ResetFilter;

	@FindBy(css = ".results-photo-item:first-child  #print-report-preview")
	protected WebElement PhotoView1;

	@FindBy(css = "#message-contact-to-builder")
	protected WebElement EmailBuilder;

	@FindBy(css = "#send-contact-to-builder")
	protected WebElement SendBuilder;

	String Error = "";

	public CommunityResult(WebDriver driver) {
		super(driver);
	}

	// Saved Searches

	// Saved searches others

	public void SavedSearchesOthers(LoginModel loginObj, ClientModel clientObj) throws InterruptedException {
		login(loginObj);
		{
			WebDriverWait wait = new WebDriverWait(driver, 10);
			wait.until(ExpectedConditions.invisibilityOfElementLocated(By.id("account-password")));
			System.out.println(clientObj.City + ", " + clientObj.StateCode);
			wait.until(ExpectedConditions.invisibilityOfElementLocated(By.id("AllNew")));
			searchTextField.sendKeys(clientObj.City + ", " + clientObj.StateCode);
			homeSearchBtnField.click();
			wait.until(ExpectedConditions
					.visibilityOfElementLocated(By.cssSelector("#hide-engage-your-clients.k-widget.k-tooltip")));

			driver.findElement(By.linkText("Close")).click();

			wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(".//*[@id='breadcrumbCount']")));

			try {
				savesearchbutton.click();
				wait.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector("#name")));

				savesearchname.sendKeys("testrv");
				wait.until(ExpectedConditions
						.visibilityOfElementLocated(By.xpath(".//*[@id='select-or-create-client']/span/span/span[2]")));
				clientdd.click();
				System.out.println("saving search For old user");
				randomclient.click();
				wait.until(ExpectedConditions.visibilityOf(savebtn));
				savebtn.click();
				System.out.println("Search Saved for the old user!");
				wait.until(ExpectedConditions.visibilityOf(okBtn));
				okBtn.click();
			} catch (Exception ex) {
				Error += "There is an error while saving listing for old Client. The exception details are mentioned here:"
						+ ex.toString();

			}

		}
	}

	public void SavedSearchesMe(LoginModel loginObj, ClientModel clientObj) throws InterruptedException {

		login(loginObj);
		{
			WebDriverWait wait = new WebDriverWait(driver, 10);
			wait.until(ExpectedConditions.invisibilityOfElementLocated(By.id("account-password")));
			System.out.println(clientObj.City + ", " + clientObj.StateCode);
			wait.until(ExpectedConditions.invisibilityOfElementLocated(By.id("AllNew")));
			searchTextField.sendKeys(clientObj.City + ", " + clientObj.StateCode);
			homeSearchBtnField.click();

			wait.until(ExpectedConditions
					.visibilityOfElementLocated(By.cssSelector("#hide-engage-your-clients.k-widget.k-tooltip")));

			driver.findElement(By.linkText("Close")).click();

			wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(".//*[@id='breadcrumbCount']")));

			// Saved search for ME
			try {

				savesearchbutton.click();
				wait.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector("#name")));

				savesearchname.sendKeys("metest");
				wait.until(ExpectedConditions
						.visibilityOfElementLocated(By.xpath(".//*[@id='select-or-create-client']/span/span/span[2]")));
				clientdd.click();
				System.out.println("saving search For ME");
				Me.click();
				wait.until(ExpectedConditions.visibilityOf(savebtn));
				savebtn.click();

				System.out.println("search Saved for the Me!");
				wait.until(ExpectedConditions.visibilityOf(okBtn));
				okBtn.click();

			} catch (Exception ex) {
				Error += "There is an error while saving Searches for Me. The exception details are mentioned here:"
						+ ex.toString();

			}
		}
	}

	public void SavedSearchNewClient(LoginModel loginObj, ClientModel clientObj) throws InterruptedException {
		login(loginObj);
		{
			WebDriverWait wait = new WebDriverWait(driver, 10);
			wait.until(ExpectedConditions.invisibilityOfElementLocated(By.id("account-password")));
			System.out.println(clientObj.City + ", " + clientObj.StateCode);
			wait.until(ExpectedConditions.invisibilityOfElementLocated(By.id("AllNew")));
			searchTextField.sendKeys(clientObj.City + ", " + clientObj.StateCode);
			homeSearchBtnField.click();
			wait.until(ExpectedConditions
					.visibilityOfElementLocated(By.cssSelector("#hide-engage-your-clients.k-widget.k-tooltip")));

			driver.findElement(By.linkText("Close")).click();

			wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(".//*[@id='breadcrumbCount']")));

			// Saved search for ME
			try {

				savesearchbutton.click();
				wait.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector("#name")));

				savesearchname.sendKeys("newclient");
				wait.until(ExpectedConditions
						.visibilityOfElementLocated(By.xpath(".//*[@id='select-or-create-client']/span/span/span[2]")));
				clientdd.click();
				System.out.println("saving search For new client");
				newclient.click();
				Random randomGenerator = new Random();
				int randomInt = randomGenerator.nextInt(1000);
				emailField.sendKeys("username" + randomInt + "@gmail.com");
				firstNameInputPopUp.sendKeys(clientObj.FirstName);
				lastNameInputPopUp.sendKeys(clientObj.LastName);
				PhoneInput.sendKeys(clientObj.Phone);
				wait.until(ExpectedConditions.visibilityOf(savebtn));
				savebtn.click();

				if (driver.findElement(By.id("save-to-listings-error")).isEnabled()) {
					Error += "There is an error while creating new client. Details are: "
							+ driver.findElement(By.id("save-search-error")).getText();
				} else {

					System.out.println("search Saved for the newclient!");
					wait.until(ExpectedConditions.visibilityOf(okBtn));
					okBtn.click();
				}
			} catch (Exception ex) {
				Error += "There is an error while saving Searches for Me. The exception details are mentioned here:"
						+ ex.toString();

			}
		}
	}

	// TODO Auto-generated method stub

	public void SRP_Basic(LoginModel loginObj, ClientModel clientObj) throws InterruptedException {
		{
			login(loginObj);
			// TODO Auto-generated method stub
			{
				WebDriverWait wait = new WebDriverWait(driver, 10);
				wait.until(ExpectedConditions.invisibilityOfElementLocated(By.id("account-password")));
				System.out.println(clientObj.City + ", " + clientObj.StateCode);
				// wait.until(ExpectedConditions.invisibilityOfElementLocated(By.id("AllNew")));
				searchTextField.sendKeys(clientObj.City + ", " + clientObj.StateCode);

				homeSearchBtnField.click();
				wait.until(ExpectedConditions
						.visibilityOfElementLocated(By.cssSelector("#hide-engage-your-clients.k-widget.k-tooltip")));

				driver.findElement(By.linkText("Close")).click();

				wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(".//*[@id='breadcrumbCount']")));

				Calendar calendar = Calendar.getInstance();
				SimpleDateFormat formater = new SimpleDateFormat("dd_MM_yyyy_hh_mm_ss");

				Screenshot screenshot = new AShot().shootingStrategy(ShootingStrategies.viewportPasting(2000))
						.takeScreenshot(driver);
				try {
					ImageIO.write(screenshot.getImage(), "PNG",
							new File("test-output\\Images\\SRP_" + formater.format(calendar.getTime()) + "_.png"));
				} catch (IOException e) {
					Assert.fail(e.getMessage());
				} catch (Exception e) {
					Assert.fail(e.getMessage());
				}

				System.out.println("The URL of SRP is " + driver.getCurrentUrl());

			}

		}
	}

	// SRP-Print Report Functionality

	public void SRP_PrintReport_Homes_List(LoginModel loginObj, ClientModel clientObj) throws InterruptedException {
		String Error = "";
		login(loginObj);
		{
			WebDriverWait wait = new WebDriverWait(driver, 10);
			wait.until(ExpectedConditions.invisibilityOfElementLocated(By.id("account-password")));
			System.out.println(clientObj.City + ", " + clientObj.StateCode);
			// wait.until(ExpectedConditions.invisibilityOfElementLocated(By.id("AllNew")));
			searchTextField.sendKeys(clientObj.City + ", " + clientObj.StateCode);

			homeSearchBtnField.click();
			wait.until(ExpectedConditions
					.visibilityOfElementLocated(By.cssSelector("#hide-engage-your-clients.k-widget.k-tooltip")));

			driver.findElement(By.linkText("Close")).click();
			wait.until(ExpectedConditions.invisibilityOfElementLocated(By.className("facets-loading")));

			wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(".//*[@id='breadcrumbCount']")));
			String txt = countField.getText();
			System.out.println(txt);
			// String oldTab = driver.getWindowHandle();

			int count = Integer.parseInt(txt.split(" ")[0]);
			if (count > 0) {
				if (driver.findElements(By.className("k-checkbox")).size() > 1) {
					// Save Listing for new client
					try {
						System.out.println("Wait started!!");
						wait.until(ExpectedConditions
								.presenceOfElementLocated(By.cssSelector("#gridContent .k-state-selected")));
						// wait.until(ExpectedConditions.invisibilityOfElementLocated(By.className("k-overlay")));
						System.out.println("Wait finished!!");
						CheckboxFirst.click();
						CheckboxSecond.click();
						PrintReport.click();
						String oldTab = driver.getWindowHandle();
						PrintReportAgent.click();
						ArrayList<String> newTab = new ArrayList<String>(driver.getWindowHandles());
						newTab.remove(oldTab);
						driver.switchTo().window(newTab.get(0));

						Calendar calendar = Calendar.getInstance();
						SimpleDateFormat formater = new SimpleDateFormat("dd_MM_yyyy_hh_mm_ss");

						Screenshot screenshot = new AShot().shootingStrategy(ShootingStrategies.viewportPasting(2000))
								.takeScreenshot(driver);
						try {
							ImageIO.write(screenshot.getImage(), "PNG", new File("test-output\\Images\\agentreport_"
									+ formater.format(calendar.getTime()) + "_.png"));
						} catch (IOException e) {
							Assert.fail(e.getMessage());
						} catch (Exception e) {
							Assert.fail(e.getMessage());
						}
						System.out.println("The URL of Agentreport page is " + driver.getCurrentUrl());
						driver.close();
						driver.switchTo().window(oldTab);
						String oldTab1 = driver.getWindowHandle();
						driver.navigate().refresh();
						PrintReport.click();
						PrintReportClient.click();
						ArrayList<String> newTab1 = new ArrayList<String>(driver.getWindowHandles());
						newTab1.remove(oldTab1);
						driver.switchTo().window(newTab1.get(0));

						Calendar calendar1 = Calendar.getInstance();
						SimpleDateFormat formater1 = new SimpleDateFormat("dd_MM_yyyy_hh_mm_ss");

						Screenshot screenshot1 = new AShot().shootingStrategy(ShootingStrategies.viewportPasting(2000))
								.takeScreenshot(driver);
						try {
							ImageIO.write(screenshot1.getImage(), "PNG", new File("test-output\\Images\\Clientreport_"
									+ formater1.format(calendar1.getTime()) + "_.png"));
						} catch (IOException e) {
							Assert.fail(e.getMessage());
						} catch (Exception e) {
							Assert.fail(e.getMessage());
						}
						System.out.println("The URL of ClientReport is " + driver.getCurrentUrl());
						driver.close();
						ArrayList<String> tab = new ArrayList<String>(driver.getWindowHandles());
						driver.switchTo().window(tab.get(0));

						// closebuttonPR.click();
						// PhotoView.click();

					} catch (Exception e) {
						Error += "There is an error IN pRINT REPORT. The exception details are mentioned here:"
								+ e.toString();
					}
				}
			}

		}

	}

	// Print Report for Communities in List View
	public void SRP_PrintReport_Communities_List(LoginModel loginObj, ClientModel clientObj) {
		String Error = "";
		login(loginObj);
		{
			WebDriverWait wait = new WebDriverWait(driver, 10);
			wait.until(ExpectedConditions.invisibilityOfElementLocated(By.id("account-password")));
			System.out.println(clientObj.City + ", " + clientObj.StateCode);
			// wait.until(ExpectedConditions.invisibilityOfElementLocated(By.id("AllNew")));
			searchTextField.sendKeys(clientObj.City + ", " + clientObj.StateCode);

			homeSearchBtnField.click();
			wait.until(ExpectedConditions
					.visibilityOfElementLocated(By.cssSelector("#hide-engage-your-clients.k-widget.k-tooltip")));

			driver.findElement(By.linkText("Close")).click();
			wait.until(ExpectedConditions.invisibilityOfElementLocated(By.className("facets-loading")));

			wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(".//*[@id='breadcrumbCount']")));
			communitytab.click();
			wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(".//*[@id='breadcrumbCount']")));
			try {

				wait.until(
						ExpectedConditions.presenceOfElementLocated(By.cssSelector("#gridContent .k-state-selected")));

				System.out.println("community tab selected");
				CheckboxFirst.click();
				CheckboxSecond.click();
				PrintReport.click();
				String oldTab = driver.getWindowHandle();
				PrintReportAgent.click();
				ArrayList<String> newTab = new ArrayList<String>(driver.getWindowHandles());
				newTab.remove(oldTab);
				driver.switchTo().window(newTab.get(0));

				Calendar calendar = Calendar.getInstance();
				SimpleDateFormat formater = new SimpleDateFormat("dd_MM_yyyy_hh_mm_ss");

				Screenshot screenshot = new AShot().shootingStrategy(ShootingStrategies.viewportPasting(2000))
						.takeScreenshot(driver);
				try {
					ImageIO.write(screenshot.getImage(), "PNG", new File(
							"test-output\\Images\\Comm-agentreport_" + formater.format(calendar.getTime()) + "_.png"));
				} catch (IOException e) {
					Assert.fail(e.getMessage());
				} catch (Exception e) {
					Assert.fail(e.getMessage());
				}
				System.out.println("The URL of Agentreport page is " + driver.getCurrentUrl());
				driver.close();
				driver.switchTo().window(oldTab);
				String oldTab1 = driver.getWindowHandle();
				driver.navigate().refresh();
				PrintReport.click();
				PrintReportClient.click();
				ArrayList<String> newTab1 = new ArrayList<String>(driver.getWindowHandles());
				newTab1.remove(oldTab1);
				driver.switchTo().window(newTab1.get(0));

				Calendar calendar1 = Calendar.getInstance();
				SimpleDateFormat formater1 = new SimpleDateFormat("dd_MM_yyyy_hh_mm_ss");

				Screenshot screenshot1 = new AShot().shootingStrategy(ShootingStrategies.viewportPasting(2000))
						.takeScreenshot(driver);
				try {
					ImageIO.write(screenshot1.getImage(), "PNG", new File("test-output\\Images\\Comm-Clientreport_"
							+ formater1.format(calendar1.getTime()) + "_.png"));
				} catch (IOException e) {
					Assert.fail(e.getMessage());
				} catch (Exception e) {
					Assert.fail(e.getMessage());
				}
				System.out.println("The URL of ClientReport is " + driver.getCurrentUrl());
				driver.close();
				ArrayList<String> tab = new ArrayList<String>(driver.getWindowHandles());
				driver.switchTo().window(tab.get(0));
				// closebuttonPR.click();
				// PhotoView.click();

			} catch (Exception e) {
				Error += "There is an error IN pRINT REPORT. The exception details are mentioned here:" + e.toString();
			}
		}
	}

	// Multiselection for cities
	public void MultiSelectLocation(LoginModel loginObj, ClientModel clientObj) throws InterruptedException {
		// TODO Auto-generated method stub
		String Error = "";
		login(loginObj);
		{
			try {
				WebDriverWait wait = new WebDriverWait(driver, 10);
				wait.until(ExpectedConditions.invisibilityOfElementLocated(By.id("account-password")));
				System.out.println(clientObj.City + ", " + clientObj.StateCode);

				searchTextField.sendKeys(clientObj.City + ", " + clientObj.StateCode);

				homeSearchBtnField.click();
				wait.until(ExpectedConditions
						.visibilityOfElementLocated(By.cssSelector("#hide-engage-your-clients.k-widget.k-tooltip")));

				driver.findElement(By.linkText("Close")).click();
				wait.until(ExpectedConditions.invisibilityOfElementLocated(By.className("facets-loading")));

				wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(".//*[@id='breadcrumbCount']")));

				LocationSearchbox.click();
				driver.findElement(By.xpath(".//*[@id='CitiesRadioFacets']/ul[1]/li[1]/label")).click();
				driver.findElement(By.xpath(".//*[@id='CitiesRadioFacets']/ul[1]/li[2]/label")).click();
				driver.findElement(By.xpath(".//*[@id='CitiesRadioFacets']/ul[1]/li[3]/label")).click();
				driver.findElement(By.xpath(".//*[@id='CitiesRadioFacets']/ul[1]/li[1]/label")).click();

				driver.findElement(By.xpath(".//*[@id='submitLocation']")).click();
				Thread.sleep(4000);
				wait.until(ExpectedConditions.invisibilityOfElementLocated(By.className("facets-loading")));
				wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(".//*[@id='breadcrumbCount']")));
				wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(".//*[@id='crumbs-bar']/ol/li[4]")));
				String text1 = driver.findElement(By.xpath(".//*[@id='crumbs-bar']/ol/li[4]")).getText();
				System.out.println(text1);

			} catch (Exception e) {
				Error += "There is an error Multilocation. The exception details are mentioned here:" + e.toString();
			}

		}
	}

	public void SRP_MultiselecionZip(LoginModel loginObj, ClientModel clientObj) throws InterruptedException {

		String Error = "";
		login(loginObj);
		{
			try {
				WebDriverWait wait = new WebDriverWait(driver, 10);
				wait.until(ExpectedConditions.invisibilityOfElementLocated(By.id("account-password")));
				System.out.println(clientObj.City + ", " + clientObj.StateCode);

				searchTextField.sendKeys(clientObj.City + ", " + clientObj.StateCode);

				homeSearchBtnField.click();
				wait.until(ExpectedConditions
						.visibilityOfElementLocated(By.cssSelector("#hide-engage-your-clients.k-widget.k-tooltip")));

				driver.findElement(By.linkText("Close")).click();
				wait.until(ExpectedConditions.invisibilityOfElementLocated(By.className("facets-loading")));

				wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(".//*[@id='breadcrumbCount']")));
				System.out.println("tESTING FOR ZIPCODE");

				Actions action = new Actions(driver);
				action.moveToElement(LocationSearchbox).moveToElement(Zipcode).click().build().perform();
				driver.findElement(By.xpath(".//*[@id='PostalCodeRadioFacets']/ul[1]/li[1]/label")).click();
				driver.findElement(By.xpath(".//*[@id='PostalCodeRadioFacets']/ul[1]/li[2]/label")).click();

				driver.findElement(By.xpath(".//*[@id='submitLocation']")).click();
				Thread.sleep(4000);
				wait.until(ExpectedConditions.invisibilityOfElementLocated(By.className("facets-loading")));
				wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(".//*[@id='crumbs-bar']/ol/li[4]")));

				String text2 = driver.findElement(By.xpath(".//*[@id='crumbs-bar']/ol/li[4]")).getText();
				System.out.println(text2);

			} catch (Exception e) {
				Error += "There is an error Multilocation for zipcode. The exception details are mentioned here:"
						+ e.toString();
			}

		}

	}

	public void SRP_MultiselectionSchool(LoginModel loginObj, ClientModel clientObj) throws InterruptedException {
		// TODO Auto-generated method stub

		String Error = "";
		login(loginObj);
		{
			try {
				WebDriverWait wait = new WebDriverWait(driver, 10);
				wait.until(ExpectedConditions.invisibilityOfElementLocated(By.id("account-password")));
				System.out.println(clientObj.City + ", " + clientObj.StateCode);

				searchTextField.sendKeys(clientObj.City + ", " + clientObj.StateCode);

				homeSearchBtnField.click();
				wait.until(ExpectedConditions
						.visibilityOfElementLocated(By.cssSelector("#hide-engage-your-clients.k-widget.k-tooltip")));

				driver.findElement(By.linkText("Close")).click();
				wait.until(ExpectedConditions.invisibilityOfElementLocated(By.className("facets-loading")));

				wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(".//*[@id='breadcrumbCount']")));
				System.out.println("tESTING FOR School");

				Actions action = new Actions(driver);
				action.moveToElement(LocationSearchbox).moveToElement(Schooldistrict).click().build().perform();
				driver.findElement(By.xpath(".//*[@id='SchoolDistrictsRadioFacets']/ul[1]/li[1]/label")).click();
				driver.findElement(By.xpath(".//*[@id='SchoolDistrictsRadioFacets']/ul[1]/li[2]/label")).click();

				driver.findElement(By.xpath(".//*[@id='submitLocation']")).click();
				Thread.sleep(4000);
				wait.until(ExpectedConditions.invisibilityOfElementLocated(By.className("facets-loading")));
				wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(".//*[@id='crumbs-bar']/ol/li[4]")));

				String text2 = driver.findElement(By.xpath(".//*[@id='crumbs-bar']/ol/li[4]")).getText();
				System.out.println(text2);

			} catch (Exception e) {
				Error += "There is an error Multilocation for School. The exception details are mentioned here:"
						+ e.toString();
			}

		}
	}

	// Print report Homes photo view
	public void SRP_PrintReport_Homes_Photo(LoginModel loginObj, ClientModel clientObj) {
		String Error = "";
		login(loginObj);
		{
			WebDriverWait wait = new WebDriverWait(driver, 10);
			wait.until(ExpectedConditions.invisibilityOfElementLocated(By.id("account-password")));
			System.out.println(clientObj.City + ", " + clientObj.StateCode);
			wait.until(ExpectedConditions.invisibilityOfElementLocated(By.id("AllNew")));
			searchTextField.sendKeys(clientObj.City + ", " + clientObj.StateCode);

			homeSearchBtnField.click();
			wait.until(ExpectedConditions
					.visibilityOfElementLocated(By.cssSelector("#hide-engage-your-clients.k-widget.k-tooltip")));

			driver.findElement(By.linkText("Close")).click();
			wait.until(ExpectedConditions.invisibilityOfElementLocated(By.className("facets-loading")));

			wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(".//*[@id='breadcrumbCount']")));
			try {

				wait.until(
						ExpectedConditions.presenceOfElementLocated(By.cssSelector("#gridContent .k-state-selected")));

				System.out.println("homes tab selected");
				PhotoView.click();

				wait.until(ExpectedConditions.visibilityOfElementLocated(
						By.cssSelector(".results-photo-item:first-child  #print-report-preview")));
				Thread.sleep(7000);
				Actions action = new Actions(driver);
				action.moveToElement(PhotoView1).click().build().perform();
				// PhotoView1.click();

				Thread.sleep(5000);
				String oldTab = driver.getWindowHandle();
				PrintReportAgent.click();
				ArrayList<String> newTab = new ArrayList<String>(driver.getWindowHandles());
				newTab.remove(oldTab);
				driver.switchTo().window(newTab.get(0));

				Calendar calendar = Calendar.getInstance();
				SimpleDateFormat formater = new SimpleDateFormat("dd_MM_yyyy_hh_mm_ss");

				Screenshot screenshot = new AShot().shootingStrategy(ShootingStrategies.viewportPasting(2000))
						.takeScreenshot(driver);
				try {
					ImageIO.write(screenshot.getImage(), "PNG",
							new File("test-output\\Images\\Photoview-homes-agentreport_"
									+ formater.format(calendar.getTime()) + "_.png"));
				} catch (IOException e) {
					Assert.fail(e.getMessage());
				} catch (Exception e) {
					Assert.fail(e.getMessage());
				}
				System.out.println("The URL of PhotoviewHomesAgentreport page is " + driver.getCurrentUrl());
				driver.close();
				driver.switchTo().window(oldTab);
				String oldTab1 = driver.getWindowHandle();
				driver.navigate().refresh();
				PhotoView1.click();

				PrintReportClient.click();
				ArrayList<String> newTab1 = new ArrayList<String>(driver.getWindowHandles());
				newTab1.remove(oldTab1);
				driver.switchTo().window(newTab1.get(0));

				Calendar calendar1 = Calendar.getInstance();
				SimpleDateFormat formater1 = new SimpleDateFormat("dd_MM_yyyy_hh_mm_ss");

				Screenshot screenshot1 = new AShot().shootingStrategy(ShootingStrategies.viewportPasting(2000))
						.takeScreenshot(driver);
				try {
					ImageIO.write(screenshot1.getImage(), "PNG",
							new File("test-output\\Images\\Home-Photoview-Clientreport_"
									+ formater1.format(calendar1.getTime()) + "_.png"));
				} catch (IOException e) {
					Assert.fail(e.getMessage());
				} catch (Exception e) {
					Assert.fail(e.getMessage());
				}
				System.out.println("The URL of HomePhotovoewClientReport is " + driver.getCurrentUrl());
				driver.close();
				ArrayList<String> tab = new ArrayList<String>(driver.getWindowHandles());
				driver.switchTo().window(tab.get(0));
			} catch (Exception e) {
				Error += "There is an error IN pRINT REPORT. The exception details are mentioned here:" + e.toString();
			}
		}
	}

	// SRP print repot communties
	public void SRP_PrintReport_Communities_Photo(LoginModel loginObj, ClientModel clientObj) {
		String Error = "";
		login(loginObj);
		{
			WebDriverWait wait = new WebDriverWait(driver, 10);
			wait.until(ExpectedConditions.invisibilityOfElementLocated(By.id("account-password")));
			System.out.println(clientObj.City + ", " + clientObj.StateCode);
			// wait.until(ExpectedConditions.invisibilityOfElementLocated(By.id("AllNew")));
			searchTextField.sendKeys(clientObj.City + ", " + clientObj.StateCode);

			homeSearchBtnField.click();
			wait.until(ExpectedConditions
					.visibilityOfElementLocated(By.cssSelector("#hide-engage-your-clients.k-widget.k-tooltip")));

			driver.findElement(By.linkText("Close")).click();
			wait.until(ExpectedConditions.invisibilityOfElementLocated(By.className("facets-loading")));

			wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(".//*[@id='breadcrumbCount']")));
			communitytab.click();

			try {

				System.out.println("Community tab selected");
				Thread.sleep(3000);
				PhotoView.click();
				wait.until(ExpectedConditions
						.visibilityOfElementLocated(By.xpath("(.//a[@id='print-report-preview'])[1]")));
				((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);",
						driver.findElement(By.cssSelector(".results-photo-item:first-child #print-report-preview")));
				PhotoView1.click();
				String oldTab = driver.getWindowHandle();
				PrintReportAgent.click();
				ArrayList<String> newTab = new ArrayList<String>(driver.getWindowHandles());
				newTab.remove(oldTab);
				driver.switchTo().window(newTab.get(0));

				Calendar calendar = Calendar.getInstance();
				SimpleDateFormat formater = new SimpleDateFormat("dd_MM_yyyy_hh_mm_ss");

				Screenshot screenshot = new AShot().shootingStrategy(ShootingStrategies.viewportPasting(2000))
						.takeScreenshot(driver);
				try {
					ImageIO.write(screenshot.getImage(), "PNG",
							new File("test-output\\Images\\Photoview-comms-agentreport_"
									+ formater.format(calendar.getTime()) + "_.png"));
				} catch (IOException e) {
					Assert.fail(e.getMessage());
				} catch (Exception e) {
					Assert.fail(e.getMessage());
				}
				System.out.println("The URL of PhotoviewCommunityAgentreport page is " + driver.getCurrentUrl());
				driver.close();
				driver.switchTo().window(oldTab);
				String oldTab1 = driver.getWindowHandle();
				driver.navigate().refresh();
				PhotoView1.click();

				PrintReportClient.click();
				ArrayList<String> newTab1 = new ArrayList<String>(driver.getWindowHandles());
				newTab1.remove(oldTab1);
				driver.switchTo().window(newTab1.get(0));

				Calendar calendar1 = Calendar.getInstance();
				SimpleDateFormat formater1 = new SimpleDateFormat("dd_MM_yyyy_hh_mm_ss");

				Screenshot screenshot1 = new AShot().shootingStrategy(ShootingStrategies.viewportPasting(2000))
						.takeScreenshot(driver);
				try {
					ImageIO.write(screenshot1.getImage(), "PNG",
							new File("test-output\\Images\\Comm-Photoview-Clientreport_"
									+ formater1.format(calendar1.getTime()) + "_.png"));
				} catch (IOException e) {
					Assert.fail(e.getMessage());
				} catch (Exception e) {
					Assert.fail(e.getMessage());
				}
				System.out.println("The URL of CommunityPhotovoewClientReport is " + driver.getCurrentUrl());
				driver.close();
				ArrayList<String> tab = new ArrayList<String>(driver.getWindowHandles());
				driver.switchTo().window(tab.get(0));
			} catch (Exception e) {
				Error += "There is an error IN pRINT REPORT. The exception details are mentioned here:" + e.toString();
			}
		}
	}

	public void SRP_MultiselectionCounties(LoginModel loginObj, ClientModel clientObj) throws InterruptedException {
		// TODO Auto-generated method stub

		String Error = "";
		login(loginObj);
		{
			try {
				WebDriverWait wait = new WebDriverWait(driver, 10);
				wait.until(ExpectedConditions.invisibilityOfElementLocated(By.id("account-password")));
				System.out.println(clientObj.City + ", " + clientObj.StateCode);

				searchTextField.sendKeys(clientObj.City + ", " + clientObj.StateCode);

				homeSearchBtnField.click();
				wait.until(ExpectedConditions
						.visibilityOfElementLocated(By.cssSelector("#hide-engage-your-clients.k-widget.k-tooltip")));

				driver.findElement(By.linkText("Close")).click();
				wait.until(ExpectedConditions.invisibilityOfElementLocated(By.className("facets-loading")));

				wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(".//*[@id='breadcrumbCount']")));
				System.out.println("Testing Multiselection for Counties");

				Actions action = new Actions(driver);
				action.moveToElement(LocationSearchbox).moveToElement(Counties).click().build().perform();
				driver.findElement(By.xpath(".//*[@id='CountiesRadioFacets']/ul[1]/li[1]/label")).click();

				driver.findElement(By.xpath(".//*[@id='submitLocation']")).click();
				Thread.sleep(4000);
				wait.until(ExpectedConditions.invisibilityOfElementLocated(By.className("facets-loading")));
				wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(".//*[@id='crumbs-bar']/ol/li[4]")));

				String text2 = driver.findElement(By.xpath(".//*[@id='crumbs-bar']/ol/li[4]")).getText();
				System.out.println(text2);

			} catch (Exception e) {
				Error += "There is an error Multilocation for Conuties. The exception details are mentioned here:"
						+ e.toString();
			}

		}

	}

	public void SRP_EmailBuilder(LoginModel loginObj, ClientModel clientObj) {

		String Error = "";
		login(loginObj);
		{
			try {
				WebDriverWait wait = new WebDriverWait(driver, 10);
				wait.until(ExpectedConditions.invisibilityOfElementLocated(By.id("account-password")));
				System.out.println(clientObj.City + ", " + clientObj.StateCode);

				searchTextField.sendKeys(clientObj.City + ", " + clientObj.StateCode);

				homeSearchBtnField.click();
				wait.until(ExpectedConditions
						.visibilityOfElementLocated(By.cssSelector("#hide-engage-your-clients.k-widget.k-tooltip")));

				driver.findElement(By.linkText("Close")).click();
				wait.until(ExpectedConditions.invisibilityOfElementLocated(By.className("facets-loading")));

				wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(".//*[@id='breadcrumbCount']")));
				System.out.println("Testing SRP Email Builder");

				driver.findElement(By.xpath(".//*[@id='mapView']")).click();

				wait.until(ExpectedConditions.invisibilityOfElementLocated(By.className("facets-loading")));
				Actions action = new Actions(driver);
				action.moveToElement(EmailBuilder).click().sendKeys("hello").moveToElement(SendBuilder).click().build()
						.perform();

				System.out.println("Testing SRP Email Builder Succesfull");

			} catch (Exception e) {
				Error += "There is an error email builder. The exception details are mentioned here:" + e.toString();
			}
		}

	}
}
