package NHSPRO8.Common.ManageFiles;

import java.util.Properties;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

public class PostMan {

	public static void sendEmail(String[] emailRecipients, String subject, String htmlContent)
	   {    

	      // Sender's email ID needs to be mentioned
	      String from = "bhiautomationmail@builderhomesite.com";

	      String host = "mail.builderhomesite.com";

	      // Get system properties
	      Properties properties = System.getProperties();

	      // Setup mail server
	      properties.setProperty("mail.smtp.host", host);

	      // Get the default Session object.
	      Session session = Session.getDefaultInstance(properties);

	      try{
	         // Create a default MimeMessage object.
	         MimeMessage message = new MimeMessage(session);

	         // Set From: header field of the header.
	         message.setFrom(new InternetAddress(from));

	         // Set the recipients
	         for (String recipient : emailRecipients) {
	        	 message.addRecipient(Message.RecipientType.TO,
                         new InternetAddress(recipient));
	         }	         

	         // Set Subject: header field
	         message.setSubject(subject);

	         // Now set the actual message
	         message.setContent(htmlContent, "text/html");

	         // Send message
	         Transport.send(message);
	         System.out.println("Sent message successfully....");
	      }catch (MessagingException mex) {
	         mex.printStackTrace();
	      }
	   }
}
