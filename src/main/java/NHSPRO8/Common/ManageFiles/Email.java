package NHSPRO8.Common.ManageFiles;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.activation.FileDataSource;
import javax.mail.BodyPart;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.SendFailedException;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMessage.RecipientType;
import javax.mail.internet.MimeMultipart;

public class Email {

	// public static void SendEmail(String[] emailRecipients, String
	// reportFileName, String Subject) {
	//
	// }

	public static void SendEmail(String[] emailRecipients, String htmlContent, String Subject)
			throws IOException, AddressException, MessagingException {
		// Sender's email ID needs to be mentioned
		Properties prop = new Properties();
		ClassLoader loader = Thread.currentThread().getContextClassLoader();
		InputStream stream = loader.getResourceAsStream("fileName.properties");
		prop.load(stream);
		String from = prop.getProperty("fromEmailAddress");

		String host = prop.getProperty("fromHost");// "smtp.gmail.com";

		// Get system properties
		Properties props = System.getProperties();
		// Setup mail server
		props.put("mail.smtp.host", host);
		props.put("mail.smtps.auth", "true");
		props.put("mail.smtp.starttls.enable", "true");
		Session session = Session.getInstance(props, null);
		MimeMessage message = new MimeMessage(session);
		message.setFrom(new InternetAddress(from));

		message.setRecipients(RecipientType.CC, InternetAddress.parse(prop.getProperty("emailCCAddresses")));
		message.setRecipients(RecipientType.TO, InternetAddress.parse(prop.getProperty("emailToAddresses")));
		message.setSubject("NHS PRO8 Report!!");
		BodyPart messageBodyPart = new MimeBodyPart();
		messageBodyPart.setText("NHS PRO8 Report");
		Multipart multipart = new MimeMultipart();
		multipart.addBodyPart(messageBodyPart);
		messageBodyPart = new MimeBodyPart();
		DataSource source = new FileDataSource(htmlContent);
		messageBodyPart.setDataHandler(new DataHandler(source));
		messageBodyPart.setFileName("Report.html");
		multipart.addBodyPart(messageBodyPart);
		message.setContent(multipart);

		try {
			Transport tr = session.getTransport("smtps");
			tr.connect(host, from, prop.getProperty("fromEmailPassword"));
			tr.sendMessage(message, message.getAllRecipients());
			System.out.println("Mail Sent Successfully");
			tr.close();

		} catch (SendFailedException sfe) {

			System.out.println(sfe);
		}
	}
}
