package NHSPRO8.Models;

public class ClientModel {
	public ClientModel() {

	}

	public ClientModel(String firstName, String lastName, String emailAddress, String stateCode, String city,
			String phone) {
		this.EmailAddress = emailAddress;
		this.FirstName = firstName;
		this.LastName = lastName;
		this.StateCode = stateCode;
		this.City = city;
		this.Phone = phone;
	}

	public String FirstName;
	public String LastName;
	public String EmailAddress;
	public String StateCode;
	public String City;
	public String Phone;
}
