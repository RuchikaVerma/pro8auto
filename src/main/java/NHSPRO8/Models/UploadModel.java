package NHSPRO8.Models;

public class UploadModel {
	public UploadModel() {

	}

	public UploadModel(String imageUploadPath, String logoUploadPath) {
		this.ImageUploadPath = imageUploadPath;
		this.LogouploadPath = logoUploadPath;
	}

	public String ImageUploadPath;
	public String LogouploadPath;
}
