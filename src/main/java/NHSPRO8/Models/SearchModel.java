package NHSPRO8.Models;

public class SearchModel {
public SearchModel(String city, String code,String minimumPrice ,String maximumPrice,String bedRooms,String bathRooms,String area){
	this.City=city;
	this.Code=code;
	this.MinimumPrice = minimumPrice;
	this.MaximumPrice=maximumPrice;
	this.bedRooms=bedRooms;
	this.bathRooms=bathRooms;
    this.Area = area;
	
}
	public String City;
	public String Code;
	public  String MinimumPrice;
	public String MaximumPrice;
	public String bedRooms;
	public String bathRooms;
	public String Area;
}
