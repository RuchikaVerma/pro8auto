package NHSPRO8.Models;

public class UserModel {
	public UserModel() {

	}

	public UserModel(String emailAddress, String firstName, String lastName, String zipCode, String password,
			String license) {
		this.EmailAddress = emailAddress;
		this.FirstName = firstName;
		this.LastName = lastName;
		this.ZipCode = zipCode;
		this.Password = password;
		this.License = license;
	}

	public String EmailAddress;
	public String FirstName;
	public String LastName;
	public String ZipCode;
	public String Password;
	public String License;
}
