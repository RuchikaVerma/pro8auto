package NHSPRO.Common.Utilities;

public class Constants {
	public static final String SignInTestCase = "Sign in";
	public static final String AboutUsTestCase = "About Us";
	public static final String CreateAccountTestCase = "Create Account";
	public static final String GuideToNewHomesTestCase = "Guide to new homes";
	public static final String HomePageLinksAndVideosTestCase = "Home page links and videos";
	public static final String MyAccountTestCase = "My Account";
	public static final String MyClientsTestCase = "My CLients";
	public static final String HeaderHelpTestCase = "Header Help";
	public static final String MyConsumerPortalTestCase = "My Consumer portal";
	public static final String SavedListingsTestCase = "Saved listings for New client and Me";
	public static final String SRP_SavedListingsSearchClients = "SRP Page saved lisitngs with searching the client name test case";
	public static final String SavedListingsHomeTestCase = "Saved Listings - Home";
	public static final String SavedSearchesTestCase = "Saved searches";
	public static final String SavedSearchNewClientTestCase = "Saved search for new client";
	public static final String SavedSearchOthersTestCase = "saved search others";
	public static final String SearchTestCase = "Perform search home page";
	public static final String FacetSearchTestCase = "Perform search SRP page with all facet applied";
	public static final String SRPBasicTestCase = "Perform SRP Basic";
	public static final String SRPPrintReportHomesListTestCase = "Perform SRP Printreport home list view";
	public static final String SRPPrintReportCommunitiesList = "Perform SRP Printreport communities list view";
	public static final String MultiSelectLocation = "Perform multiselection for city on SRP";
	public static final String SRP_MultiselecionZip = "Perform multiselection for ZIP on SRP";
	public static final String SRP_MultiselectionSchool = "Perform multiselection for School on SRP";
	public static final String SRP_MultiselectionCounties = "Perform multiselection on SRP";
	public static final String SRPPrintReportHomesPhoto = "Perform SRP Printreport home photo view";
	public static final String SRPPrintReporCommunitiesPhoto = "Perform SRP Printreport communities Photo view";
	public static final String SRPEmailBuilder = "EMAIL BUILDER";
	public static final String SRPLinksAndVideosTestCase = "SRP page links and videos";
	public static final String CD_Basic = "CD BASIC Page testing";
	public static final String CD_EmailBuilder = "CD page email to builder";
	public static final String BacktoSearchResult = "CD Back to search navigation test";
	public static final String CD_RequestAppointment = "CD request appointment";
	public static final String CD_PrintReportCommunity = "CD Print report community agent and client version";
	public static final String CDLinksAndVideosTestCase = "Testing community page links and videos";
	public static final String CD_HomesPrintReport = "CD Print report homes agent and client version";
	public static final String CD_Gallery = "CD gallery view for images and navigation testing";
	public static final String CD_Gallery_FIV = "CD gallery fiv view of images testing";
	public static final String CD_Gallery_Videos = "CD Gallery view for videos testing";
	public static final String CD_Gallery_Videos_FIV = "CD Gallery view for videos for FIV testing";
	public static final String CD_Gallery_OtherMedia = "CD Gallery view for other media testing";
	public static final String CD_GalleryFloorPlan = "CD Gallery view for other media testing";
	public static final String CD_Gallery_OtherMediaFIV = "CD Gallery view for other media testing";
	public static final String CD_MapandDirection = "CD Page Map and Direction testing";
	public static final String CD_SavedLisitngs = "CD Page saved lisitngs for clients test case";
	public static final String CD_SavedListingsMe = "CD Page saved listing for Me test case";
	public static final String CD_SavedListingsSearchClient = "CD Page saved lisitngs with searching the client name test case";
	public static final String CD_SavedListingsRandomClient = "CD Page saved lisitngs with Random Client i.e any old client test case";
	public static final String HD_Basic = "HD BASIC Page testing";
	public static final String HD_EmailBuilder = "HD page email to builder";
	public static final String HD_RequestAppointment = "HD request appointment";
	public static final String HD_VerifyImageVideos = "Testing Home Detail page links and videos";
	public static final String HD_PrintReport = "CD Print report community agent and client version";
	public static final String HD_SavedListingsNewClients = "Saved lsitings in home detail page for a new client";
	public static final String HD_SavedListingsMe = "HD Page Saved lsitings in home detail page for Me";
	public static final String HD_SavedListingsRandomClient = "HD Page saved lisitngs with Random Client i.e any old client test case";
	public static final String HD_SavedListingsSearchClients = "HD Page saved listing with searching a client";
	public static final String HD_Gallery = "HD gallery view for images and navigation testing";
	public static final String HD_Gallery_FIV = "HD gallery fiv view of images testing";
	public static final String HD_Gallery_OtherMedia = "HD Gallery view for other media testing";
	public static final String HD_Gallery_OtherMediaFIV = "HD Gallery view for other media testing";
	public static final String HD_Gallery_Videos = "HD Gallery view for videos testing";
	public static final String HD_Gallery_Videos_FIV = "HD Gallery view for videos for FIV testing";
	public static final String HD_MapandDirection = "HD Page Map and Direction testing";
	public static final String HD_FloorPlan = "HD Page floor plan testing";

}
